### **戏精进阶**
> 作者:A神asking   发表时间:2019-02-20 20:00:00   原文: [点击跳转](http://mp.weixin.qq.com/s?__biz=MzUyNTM3NDk5MQ==&amp;mid=2247484627&amp;idx=1&amp;sn=185d0d772eafad0c92ce66166620cc89&amp;chksm=fa1e47b3cd69cea5fe63d5a0df503c0e098845035e51932e720edc49e6d5f600ce0a7b8ac277&amp;scene=27#wechat_redirect) 

---
一直陆陆续续有新老朋友来，大家也蛮关心A叔的近况,可能大家都知道我的经历，现在当然早过了财务自由的阶段，这两年过着炒炒股票，打打德州，偶尔澳门一日游，腻了就去海上漂几天的日子。
股市没有神，我说的也不尽然全对。公号取名的时忘了告知小友助理，用A叔就好不要用神。所以以后大家留言，客气的话叫我声A哥或A叔就很好。
这里主要分享心得和交流逻辑，股市是一个自我修炼的地方，前辈们也最多给你们一些提醒，让你们少走点弯路，最大的敌人还是你们自己。而你会发现其实最终能帮你的也只有你自己，我不行！养家不行！小赵也不行！

![](./imgs/1585427334.0757709.png) 

开盘
柔性电子国风塑业一字七板。分支纳米银乐凯胶片一字二板。杨子新材一字首板。
农业一号文件益生股份等多股一字首板。
农业广弘控股换手二板，农业多股炸板。
前高度板顺灏股份一字三板。前人气全柴动力快速上板。
次新明阳智能下杀，恒铭达跟跌，农业利好快速板新乳业炸板。
纳米银万顺股份首板。深天马A冲高横盘后回落。
柔性电子华映科技卡位锦富技术，率先反包二板。金明精机换手四板。
燃料电池全柴动力炸板。
明阳智能跌停，恒铭达跟杀。

午后
柔性电子分支纳米银长江润发首板。
5G东方通信杀绿后拉起。同洲电子首板。
银星能源换手后反包首板。
5G路通视信等助攻首板，东方通信回封。
前人气风范股份回封。
全柴动力跟风冲板回落。
柔性电子深天马A京东方A冲高后小幅回落。金龙机电反包，西部材料首板。

华映科技上板后金明精机仓控买点。银星能源午后打板试错。

情绪逐步回落
指数高位震荡临调整
主线柔性电子分支及低位补涨，即将洗牌 。

整体仓控，等待情绪转折。
明日继续考验动态博弈能力。
换手交易性机会仓控试错，不限于柔性电子。

风范股份，银星能源午后先锋。东方通信再次刷爆眼球，戏精进阶老戏骨，影帝大奖实至名归。

提示各位朋友：本号不推荐任何个股，不加个人QQ或者微信，亦没有QQ群或者微信群，不收任何形式的会员，无任何收费项目，本股市有风险，投资需谨慎！！

![](./imgs/1585427334.2298794.png) 

我每天耐心的教，小友们有没有收获？
如果实在看不懂，就慢慢学。不懂又没耐心的话，劝君取关不浪费时间，因为炒短线不一定适合你。
如果有收获，可以点个赞，并帮我推荐转发给短线的同好，一起积累福报，也让我能够得偿回馈市场的心愿。

大家有问题可以踊跃留言
好的留言我基本都会回复

留言区有黄金！

![](./imgs/1585427334.374097.png) 


---
> FOCUS:   
我是做强势股的，这一轮没赚钱，OLED买不进，其他的看不上，结果前期牛股反抽高度超乎想象。类似的情况怎么加强？    
>> 作者:   
做大局推演，不断贴近市场。
 

> buffros.李:   
A师，今天毛估估凯盛科技9个亿成交量，这么多反包的老龙头，在龙头地位之争过程中OLED概念理应反包机会吧。不知道毛估估的成交量与您的测算差距有多少？请赐教    
>> 作者:   
最近高位板放量要放宽些，新闻联播一播，僵尸户都在回忆密码了，多出来很多抛压。
 

> ？:   
A师，像今天这样的恒铭达，突然大单砸死如何避免，昨天开盘进的，预期能封板，结果吃面出来的！[呲牙][呲牙][呲牙]    
>> 作者:   
说过次新套利阶段概率很大，柔性电子主线还在，另一条线在发酵老人气反抽，次新抢不过资金。
 

> 笑而不语:   
A哥，今天尾盘有点蒙，从盘面看是银星带动东方，还是东方带动银星？怎么判断啊？求上墙    
>> 作者:   
相互壮胆[呲牙]
 

> 晓山:   
请教，在龙头可能挂掉的情况下，金明精机这样的四板中位股，为什么还可以打呢？    
>> 作者:   
低位也有补涨需求，之前爆过量，上板前看到承接么
 

> 小肥兔:   
你说的怎么那么对新韭菜不去做怎么知道对错我不怕赔钱就怕买些边缘板块浪费好几年时间什么也学不到给自己几年时间试试看 王侯将相宁有种乎    
>> 作者:   
先少些钱学，被红烧肥兔就看不到你了。
 

> 猪头:   
沙发！东方不败精彩绝伦    
>> 作者:   
手术做得好，男女老少都爱
 

> Shy:   
A叔晚上好。昨晚复盘时想到了3128会低开的可能，主要还是前段时间一直看好的749放出来后尴尬地低开这件事让我觉得低开也是一种正常的、能够接受的可能，因为有这样的准备，所以低开后也没有直接出，十点半之后板块下杀，看了看杀跌幅度没别的板块热门股大，还是忍住没出，并于中午收盘前又在5.18小买了一些，赌有资金模仿昨日下午的路数，下午见上攻无力，只好在5.54将昨天买的出完，虽然盘中一直忐忑，但因为运气好最后还是收获了满意的结果，同时自己也多了一次忍受波动的体验。中午看A叔文章说柔性电子一波流概率极小，午后小仓位低吸了中军深天马A，之前的京东方A也持股未动。想请教A叔的是，进入防守周期后低吸这些相对稳健的板块大盘股算不算一种风报比较合适的防守策略？    
>> 作者:   
最佳的防守策略是仓位
 

> 人间:   
A兄怎么看全柴动力？    
>> 作者:   
昨天前天过高没放量，否则可以封住。
 

> 执念:   
A哥，凯胜科技水下割，竞价板块全水下，给高溢价不砸，脑子又water了[捂脸]上午看防守，冲高出所有，银星SB仓控打回，明天看二波情况加仓。    
>> 作者:   
凯盛开盘这么顶是逼人砸盘😂
 

> 宫生顺:   
A叔，今天看华映科技卡位锦富新材，直接跟随资金扫板，然后反复炸板，瑟瑟发抖中。请问A叔怎么看纳米银的高度，明天是否值得接力呢？    
>> 作者:   
会有部分补涨，高度要看国风塑业开板后的板块调整强度
 

> 白:   
a叔今天顶了002565是什么原因呢，不怕高吗    
>> 作者:   
影帝的反包示范效应
 

> 罗土木:   
戏精我没有赚一毛钱，也没有买过，在银星呆着    
>> 作者:   
银星昨天估要多调整一天，今天直接被拱起来
 

> Erik Chiu:   
A叔，我今天平盘进的锦富技术，见其几度欲上攻，但始终冲不破五个点。那些要是打板大富的更是吃大面。请问a叔，为何资金今天不选择锦富技术呀，大资金一直流出    
>> 作者:   
去看前几天龙虎，对比两者的持仓情况，接力资金选择了华映科技安全性更高。
 

> 铷妍:   
银新能源只敢试错买了一点，没敢打板！东方通信7个点早上卖，下午看银星能源封板，不对劲，又6.5个个点买回来...比较闲得慌！2封打板试错了华映科技。想的是资金会否启动低位的中军接替深天马。昨晚就想，如果金明精机4板去不去，后来考虑高位挂的话，4板为中位股，高不成低不就意义不大，就没去。其实我想打乐凯胶片2板做高低切换的，但反应慢了一下没去成？京东方过不去高点卖光了。今天收缩仓位修正一下。老师，您认为乐凯如果买到的话，逻辑对吗？002947吃了小肉，吓死人！    
>> 作者:   
看到消息的话，结合盘口可以小试。
 

> Mr.AleX:   
A哥好，看了复盘需要的动态博弈能力还差好多，今天看情绪不对直接选择撤退了。 华映科技竞价超预期后卡位上板的解读精妙，看到了他，就敢上金明精机了。 请教下凯盛科技的理解，昨天没有放出安全的量出来，只能算是个半放量加速板，因为有国风塑业在，所以今天高开超预期就是先卖的点？    
>> 作者:   
凯盛昨天秒炸就放了5亿，直接堵死还有希望，放出口子还那么高肯定是抛压喷出
 

> 田术全:   
叔！今天竞价农业股多股一字，庄园牧场开9%多，想打个卡位板.就直接扫了。是因为一字太多沦为后排，还是大湾题材一日游影响？还是埋伏资金砸盘？    
>> 作者:   
农业看图形潜伏不少，而且持续性向来不好，筹码干净的新乳业都砸死
 

> 海天一色:   
A叔，为什么同样的前人气股000622没有风范这些股强，谢谢    
>> 作者:   
时间近的资金记忆更强些
 

> cscy:   
京东方干不过深天马。预判Oled今天会冲高回落，锦富技术竞价不及预期放弃开盘低吸京东方做T，凯胜直线下杀。冲高卖出一半京东方，实在没有锁仓资本，借着冲高减仓提高安全垫。请教A叔当发现深天马更强时想切换但是买点不舒服怎么办。京东方人气也不错就是力度不行。谢谢    
>> 作者:   
已经很努力，拉起来就被高抛T😂😂
 

> 文甫:   
跟A师半年，自觉有提高，但看盘能力差距仍大，每天盘后必学，希望A师千万保留公众号。令，周五买进600776东方通信，至今锁仓，得益于A师教导。    
>> 作者:   
自己的功劳[强]
 

> fancy:   
早上看到转势板锦富开盘不及预期，预判资金柔性屏分歧后回流粤港澳，遂低吸华金资本，锂电池起势后吸贤丰控股，收盘发现全是自己的意念盘[捂脸][捂脸]，请教A兄，这种操作思路可行否?还是有一些我没注意到的因素，其实并不应该这么操作?    
>> 作者:   
整体思路非主流😂
 

> 至爱康小妖:   
A叔，我已经看透你了 你嘴上说着让我们苦练一招 结果你是七十二绝学面面俱到的讲 你这是把大家往天才的理解力上逼是吗？ 也就我这样骨骼惊奇是万中无一的炒股奇才（这是你说的[耶]）才能识破  叔，其实我只想问问你如何从盘面的蛛丝马迹每一步都对情绪拿捏的如此准确的?    
>> 作者:   
也经常看错啦[捂脸][捂脸]
 

> Huaw.orz:   
a叔，请问您一个问题。我是去年宏川智慧那会入市的，近一年里我理解的做法都是超短龙头。关注了您的公众号之后才有机会学习市场的各种周期。也正是因为我入市到现在，大盘基本就没过几天成交量像最近一样稳定在2000亿以上。按以往的经验，今天应该是龙头见顶，准备空仓看戏的节骨眼。但是，我没遇到过这么大的成交量，和这么凶的做多龙头的情绪，特别是在以往要变节的点上。所以今天下午眼睁睁的看着银星封板也没出手试错，感觉买多少都不合适。我想请问一下a叔，遇到这种该弱不弱好几天的行情该怎么办？做自己看得懂的，但是最近，特别是今天看不懂后就感觉浑身不对劲。好像掉队又好像过时了。    
>> 作者:   
年后的市场和前两年都不同，创业板已经放了三倍量。
 

> 曾令奋:   
请问A师:今天平盘买锦富科技，明天能否顺利抽身。谢谢！    
>> 作者:   
859顶死的话应可以
 

> 董二家的大小姐:   
今天还挺尴尬昨天买的精明没赚这家伙名字里2个精真是精啊昨天这么强居然今天平开后来看匆匆走了之后把剩下的银新10点多走人尾盘都板我却空仓发呆了东方通信下午可以上吗好几个5g上板！    
>> 作者:   
戏精基本是独立走股性
 

> 小肥兔:   
叔其实我是复盘了创投的走势很多股票都是几个涨停以后缩量横盘几天继续第二波拉升我是认为柔性屏这一波的情绪资金不输给创投也是开板了后横盘震荡于是我就进了比较小盘子的香山。。。。。。然后就套的不要不要的不过我还是不死心我要等明天看看    
>> 作者:   
输赢都赚经验[呲牙]
 

> 展望未来:   
a叔，请问002946的回报怎么看？明天有反包的可能吗    
>> 作者:   
回报还可以，正常要磨几天才能反包
 

> 执念:   
A哥，还有一件搞笑的事，我这几天分别低吸了明阳智能，鹏起科技，华明装备，全部是涨停前两天买入，涨停板前一天卖出[捂脸]造化弄人啊    
>> 作者:   
[捂脸][捂脸]
 

> 篮球:   
A哥好今天华映用了SB大法。银星11号看到竞价留一线直接涨停仓控买进18号开盘出有溢价在手确实会拿久些之前一晃动就出去现在晃出去后反手追回也快自选剩40多个了[偷笑]谢谢    
>> 作者:   
[强][强]
 

> 得与失:   
再把这个资金博弈期梳理通了，对此阶段预判的经验应该就完善了，体系相对空白区。凯盛水下跟着万手单割，一点也不冤枉啊，亏认知的钱，我预估竞价还是符合预期呢[呲牙]，当成争龙呢之后砸太深想等回流后的冲高走，后面见万手单就只好跟着砸了    

> 索菲亚:   
A师，打板了万顺，希望明天能有好收成[呲牙]    

> 立:   
A叔好，市场情绪在，资金充沛，但没有好题材去，就会去银星能源、东方通信这样的人气股，这样理解对吗？    

> 介升:   
A兄好，回归市场，今天操作很差。 1.早盘打板0023，明天准备吃面了。逻辑是，昨天收盘，板块阵形是最好的，早盘披板块竞价还好，粤泰、穗恒运A一字，于是上板打的。事后来看，上板的换手是问题，没达到自己预估的量。 2.明天不准备进场，因分析情绪开始退潮。    

> ...:   
A叔，发现高位国风塑业这种临界开板并且其他高位股回调时，中位股像金明精机这种上班就需要下面地位的助攻确认后才能仓控上，单纯的位差并不足以支撑中位股顶住高位回调压力    

> 雪峰:   
哥今天早盘一键清仓了空仓中等待龙头见顶之后再回来    

> 張、學:   
本周围绕主线操作三天收益5%,不嫌少。技术练到家了钱自然就多了    

> 菲林:   
叔，这几天真的太累hh今天早上起床迟了30分钟，明阳智能，恒铭达割肉走人，15点大肉变成8点。。。。果断调整一下发现次新死，OLED也不行直接上车总龙头，银星能源。仓位控制下来了但是很郁闷。。。。。。。。今天晚上早点结束早点睡觉了。[啤酒][啤酒]新乳业是重仓，其他2个仓控打的板。    

> 小金蟾~不干杂毛:   
为了买到LOED，蹲到的全柴动力和风范都卖了，弄了三板的华映，五板的东材，结果都割，还不如呆在妖股里，这波炒作完败，想想是被LOED带沟里，核心股没有搞到，还得学习。    

> 曹xq:   
汇报师傅：两天主要做了大的政策消息，出0088，打板300189，600371，转点溢价罢了，不过371尾盘抛压厉害，感觉市场情绪题材都到高潮后期了，反面的东西多了，需要做个保守派了，    

> 小谦:   
不过我感觉今天东方完全是被溢价打上去，龙头水落石出在情绪下降，明天大跌还有机会，如果大涨八九是坑    

> 庆敏:   
自己每次都做筹码集中的，盈利曲线还凑合，自从年后关注了A叔，简直弱爆了[流泪][流泪][流泪]以后逐步跟A师好好研究下热点，希望可以进步[愉快]    

> 金798:   
20190220复盘 早盘 农业板块受消息刺激直接高潮，该板块潜伏盘较多，应又是一日游 国风塑业竞价一字 凯盛科技竞价＋9，开盘后下杀翻绿 柔性屏幕题材其余票竞价低开，整体氛围较差，进入板块末期。 大盘同步调整 粤港奥题材及强势股全面熄火 午后 银星能源东方通信先后涨停，带题材股作最后的挣扎，但后排没有跟随，情绪进入退潮期。   结论 早盘低开下杀超预期，情绪、题材及大盘均进入退潮初期，已分批清仓锦富，但清早了点，接下来应空仓观望，等待下次机会。 也可等待下跌动能释放完毕再寻买点。下一个买点或许又变成新的题材了。  对比A哥复盘哪些主要方面未发现或未理解到：大体相同，情绪末期不敢买，也存在没发现控仓买点之不足。    


