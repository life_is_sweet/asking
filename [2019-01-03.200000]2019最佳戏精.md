### **2019最佳戏精**
> 作者:A神asking   发表时间:2019-01-03 20:00:00   原文: [点击跳转](http://mp.weixin.qq.com/s?__biz=MzUyNTM3NDk5MQ==&amp;mid=2247484493&amp;idx=1&amp;sn=ce8af9d4305f1240a754861db60fdd97&amp;chksm=fa1e472dcd69ce3b7a8ea538e004b855b458d1dfa5937ac44adcdfc885ea95c1421bb88812dd&amp;scene=27#wechat_redirect) 

---
先提示各位朋友：本号不推荐任何个股，不加个人QQ或者微信，亦没有QQ群或者微信群，不收任何形式的会员，无任何收费项目，本股市有风险，投资需谨慎！！

![](./imgs/1585427439.9951434.png) 

早盘
特高压5G新宏泰竞价板。
达安股份红开后下杀。
特高压太阳电缆，中元股份首板。航天通信涨停，引发军工联想。睿康股份，烽火电子，天海防务等助攻。次新长城军工助攻。军工高潮。
方正证券，国海证券领衔券商护盘。
东方通信跳水，达安股份同步跌停，带崩5G。
泰永长征跟随跌停。
通产丽星放弃卡位良机，跳崖殉情，带崩创投。次新板块跟跌。
风范股份分歧转一致五板。
5G小盘欣天科技率先发弹，带动东方通信翻红。
5G反抽后，军工航天动力，航发科技，贵航股份，金盾股份等先后炸板。
军工特高压5G通光线缆反包。

午后
银邦股份助攻军工。
特发信息二板助攻5G，东方通信企稳反弹。
金利华电，国电南自助攻特高压。
东方通信尾盘上板，东信和平同步助攻。

通光线缆反包板，平潭发展借势军工低吸套利，风范股份龙头三买。

昨日东方通信反身卡位通产，今日假摔碰瓷通产，2019最佳戏精。

谨慎5G，创投，近端次新。
上证护盘，深指已新低，情绪仍亢奋。
结论：开始试错。
关键词：低位，新题材，复合题材，筹码博弈。

看戏精继续演技爆棚


![](./imgs/1585427440.149574.png) 

我每天耐心的教，小友们有没有收获？
如果实在看不懂，就慢慢学。不懂又没耐心的话，劝君取关不浪费时间，因为炒短线不一定适合你。
如果有收获，可以点个赞，并帮我推荐转发给短线的同好，一起积累福报，也让我能够得偿回馈市场的心愿。

大家有问题可以踊跃留言
好的留言我基本都会回复
留言区有黄金！

![](./imgs/1585427440.3091476.png) 


---
> 假如我摸鼻子会下雨:   
A叔，像今天这种多条主线崩盘了，资金还仍愿意继续干的情况下，是会选择新主线类似军工特高压还是干总主线5G？资金在什么情况下会不管筹码大举干呢？    
>> 作者:   
譬如中午喝多了[呲牙]
 

> 苏健:   
东方通讯拉高跳水，顺带拍拍通产丽星肩膀“我跳了，接下来看你表现了”。随后有资金极速压价出货，没格局？还是对市场的敏锐？    
>> 作者:   
和A股一样，涨不过道琼斯，一定要跌得赢[捂脸]
 

> 陈跃明:   
A哥，我记得军工持续性好像基本不过3板，那题材集合5G，高特压，航天的可以重点关注咯？价格貌似低价更好。    
>> 作者:   
所以说要多题材，譬如叠加远端次新和填权。没有好题材就搞筹码博弈，博弈出来了再安题材上去。
 

> 安东:   
A叔，是因为情绪仍然高涨，指数破前低，风险已经释放。所以，可以试错吗？    
>> 作者:   
前低的恐慌盘面下不弱反强，大概率下一波又是无缝接
 

> 股海沉浮:   
A大，每天复盘怎么找出目标股，第二天的买点怎么把握啊，恳求您指点，谢谢，比如今天，新宏泰，鹏起可以打板吗，又害怕炸，再次感谢!    
>> 作者:   
统计概率，根据情绪不同阶段调整买点。不打
 

> 小肥兔:   
统计概率，根据情绪不同阶段调整买点。 叔你知不知道你这一句话 是股票的多少精髓 你轻轻一句 够多少人悟几年 也许还悟不出    
>> 作者:   
[啤酒][啤酒]
 

> 双下巴:   
这戏完全出乎意料，关键把小弟也给骗了    
>> 作者:   
达安才是正常走势😂
 

> 董二家的大小姐:   
谢谢叔叔这里我的疑惑就是达安算连板高度还是把东方通信算连板高度因为泰永压了空间后达安这么走也算合理但达安这么走风范多尴尬的6回到5那今天这个5的位置怎么看呢上次超华科技7变6打死了光一的4森远的5这次如果还是这个规律那明天风范合理的走法应该是就一个集合高开的钱吧还是说东方通信算空间板然后这里只不过是军工特高压卡死了5g类似创投卡死了人工智能光伏叔叔这里我迷茫的是到底是a板块卡死了b板块还是在杀空间板情绪退潮不是题材问题是空间问题希望能指点江山    
>> 作者:   
回到五无需参考，因为不做高位接力。东方三倍高度和达安六板空间已经够搞低位了
 

> 至爱康小妖:   
A叔，我今天真是无语了，眼看着东方不败跳水，通产丽星这时候分时是向上的啊啊啊啊，量到了有木有……筹码捋顺了有没有，分时强支撑有木有……我要是资金我也不会放过这机会啊，果断上啊……结果……真是一句MMP……A叔，今天真是让通产丽星做伤了……无力吐槽……我想来想去都没有错!你说是不是，叔？    
>> 作者:   
已经在这个位置了，打板更确定，不少这一个点。看当时的量就算板了也有二封机会。
 

> Mou:   
欣天科技会不会接力5g呢？早上盘面看的我瑟瑟发抖。    
>> 作者:   
5G板块结构已定型，怎么接力😂
 

> 初一:   
今天午后地板抄了一点达安股份，后来想想确实做错了，即使换手够了但在情绪退潮期间，非核心龙头，人气也不是很高。明天会被核么？    
>> 作者:   
先求东方不低开
 

> 董二家的大小姐:   
叔叔晚上好风范也有无人机属性吧属于军工的这票从2板开始一路暗牌打到5板真是没谁了早上冲高走了看到达安大跌不敢去5板的分歧转一致被通光伤害过又是风范的跟风一犹豫也没人然后一天就发呆了哎    
>> 作者:   
今天上不动仓位
 

> 鑫鑫知我鑫:   
老师：鹏起科技能3进4吗？    
>> 作者:   
看庄是不是倒货
 

> 茶叶蛋:   
a叔，达安股份明天还能活着出来吗？今天翘板是资金为了出逃？有点看不懂    
>> 作者:   
东方带开板
 

> 多多:   
a叔好。今天淡定的上了通光线缆。像航天通信这类快板总是抓不住，凭的事反应力吗？    
>> 作者:   
经验。航天军工里机构多，炒得少没经历就难抓
 

> 向乾！:   
a叔，次新都跌成这样，快到试盘时间了吧，    
>> 作者:   
还早
 

> 微笑的我:   
好看好看[抠鼻] 点个好看你点不了吃亏[阴险] 点个好看你点不了上当[傲慢] 点个好看牛股千千万[得意]    
>> 作者:   
[啤酒][啤酒]
 

> Mr.AleX:   
A哥好，今天操作层面和哥理解一致，理解方面还需指教： 1、纯技术买点鹏起科技算，可惜是庄股，算今天核心之一，带起了军工，也有资金尝试了稀土。 2、航天通信是正常5g+代码演变到军工，超预期卡位，感觉有作盘资金引导。 3、新宏泰一字超预期、睿康股份也是助攻特高压。 4、军工起，直接助攻风范股份有个无人机题材，龙头地位坐实，问题是泰永6班，他5班，今天还敢顶板的，是有资金赌他成妖？周期点精准衔接泰永长征26日开板日？上海票，监管要强点也是弊端，但是试错买点应该对，仓控一笔可以。 5、后面反应过来的资金打了通光线缆，试过抛压烂板也就3.68亿，题材筹码结构有投机优势，很好的套利标。 6、看到泰永长征、达安股份、东方通信的负反馈，想起了哥讲的见顶日手慢点要，风范股份是反应过来后相通了逻辑，看到了通光线缆选择了排版后未遂，心态就是套利买不到也不可惜。 7、很多人说风范股份被年线压制，是否这类只需要通过放量来解决就好？ 8、欣天科技在东方通信杀跌后还拉升当时难理解，属不甘心资金的所为，也说明5g树大根大，下午指数杀跌无去处还得抱团，    
>> 作者:   
四轻抛压下筹码博弈的产物。七今天已经测试爆量，压力在于长征的准七板和达安的六板。
 

> 执念:   
A哥，我对今天的三个开仓点有点疑问。 1、通光线缆套军工、特高压，东方通信反包高度？ 2、早上军工炒作内核是两岸，结合承接，所以低吸平潭发展？平潭上板后我才反应过来，不然我就扫了。 3、风范股份分歧转一致之前的分时，为什么我总感觉怪怪的    
>> 作者:   
一通光和5G联动一般。2300和600677都是电缆，再借势军工。二是。
 

> 珈豪:   
特发信息二板助攻5G，东方通信企稳反弹。  老师，看你文中字体加粗，肯定是看盘重点，想问一下特发信息有军工和5g概念，怎么你就说他是助攻5g呢？    
>> 作者:   
借势军工，带动5G。
 

> 红尘来客:   
哥好，低吸平潭发展套利除了题材是否也与其首板量足有关？    
>> 作者:   
是
 

> 大树:   
请教A师，通光线缆27号遇前期高点破板，当天套牢2亿+，今天反包成功；是否可以理解为1.2日红盘解放12.27日套牢，今天开盘第一波拉升至9.10左右及下跌洗筹后换出12.27日套牢盘，后借力军工拉板。不知理解对否？    
>> 作者:   
[啤酒]
 

> 行者:   
A叔，这才年初啊，定义最佳是不是太早了    
>> 作者:   
还有更佳😂
 

> 介升:   
A兄好，今天还是没有操作。 1.竞价看到特高压新宏泰一字卡2板，太阳电缆上板，第一反应是低吸风范股份，但因计划不交易没出手。想问（排除计划原因）这个判断逻辑上是否可行？ 2.市场该弱不弱，我认为就是强，明天准备出手，基于情绪与接力氛围小转暖，小仓试错，我主要做接力还是考虑以热点多重概念叠加为主，这个策略有没什么问题，望赐教。    
>> 作者:   
一确定性低。二复盘文有提
 

> 感动人生:   
A哥好！竞价发现中元不错回踩一下带量拉升就进去了然后上亿资金封板，我发现量不足，肯定会炸板补量！然后一直没人封板，有人点火也没有封板资金！下午用其他资金打板金利华电！是抛压太大还是特高压没地位？谢谢A哥指点[啤酒][啤酒][啤酒]    
>> 作者:   
通产跳崖后，创投属性带开的
 

> 老邓:   
10元以下涨停30只，15元以上涨停3只，东方不败亦是由4块多启动，如同德新那一波主升，又慢慢回到了低价超跌了。目前看，情绪先于指数启动，那么同步也快了。    

> 林少斌:   
本来指望今天5G调整的话创投能接，昨天搞了点通产丽星，结果就被摁了，看了下龙虎榜，赵老哥亏着出的，歌神倒是挣了，跑得倒是快得很........    

> 龙宇:   
A叔辛苦[咖啡] 这波感触很深，炒股还是要跟政策，通讯板块频谱划分加低价股鼓励并购重组，鼓励回购股票，在指数天天新低的情况都跑出东方通信，南京熊猫牛股。超低个股就是实实在在的利好，股价跌去百分之九十，破净的比比皆是，屡次鞭尸都挺住，稍有利好就好估值修复，节奏看下周将要启动两会行情，估计低价属性还有很多翻倍黑马[转圈][嘿哈][嘿哈][嘿哈]    

> 之乎者也:   
叔，昨晚预判达安挂了，776也兜不住，早盘达安一下杀到负五，我就想看看776如何演绎，只是猜中了开头没猜到结尾啊[捂脸][捂脸]，这种庄股真是太流弊了[强][强][强]    

> 樱桃:   
a叔晚上好，集合以为鹏起小红叶呢，开盘就上了，哦，平潭发展也上了[捂脸]    

> 一米阳光:   
A叔晚上好，今天操作止损昨儿低吸的5g加仓昨天打板的乐视。晚上在叔这里理解市场    

> 博弈:   
风范股份三板扫进，今天五个点出了，封板只敢扫回一半。平潭发展感觉观察半天不见启动，感觉还是等上板再去比较安全，最后一笔实在太猛，还是慢了点。    

> 李飞建1505175187X:   
市场真是永远在变，又流行庄游股了。 几乎都是烂而不弱的走势，今天昨日涨停的指数嗨爆了，感觉低位的真可以去。    

> 曹xq:   
弱转强超预期是情绪操作中相对不变的东西，尤其在龙头中表现强烈，需要深度理解，善于运用。    

> Lionger:   
单独的新闻还不够，需要搞两条来拼凑，新闻1+1模式。鹏起科技：起于股转叠+军工；太阳电缆，瑞康股份，通光线缆：特高压+军工；特发信息：军工+5G。    

> Rui:   
佩服昨天点火航天通信的游资，真是先手无缝切换啊!军工纯游资和散户情绪，看不到机构和庄乘势合力参与，所以今天烂板多，明天正溢价较难，今天想试错一笔长城军工，转念想起昨天A大提醒次新未到，放弃。5G尾声了，东方不败前半场走势昨天预测对了，后半场简直亮瞎眼了;分化无操作，明天静观如何退潮。特高压是在5G太名明牌分化，军工无合力的情况下异军突起，通广线缆手慢，买了1成东方铁塔（后悔）；平潭发展无脑上了一笔。今天对A大老提的各题材与个股在盘中的市场地位有了体会，由衷感谢    

> 刘建华555:   
市场的情绪银明显，早盘东方通信一跳水，东信和平，南京熊猫跟着跳，可能是看到军工起来了，所以东方通信跳水，后面看看军工炸了好多板，东方通信又拉起合力上去了，东信和平，南京熊猫也跟着上，最后东方，东信上板了，而熊猫市场地位不够，冲高回落    

> 向阳花开:   
接下来几天强势股开始杀人了吧    

> 难有二三:   
A叔好。每天来学习略有所获，早盘鹏起秒板、太阳电缆，中元股份首板。航天通信涨停，引发军工睿康烽火天海助攻随后看到次新长城军工拉板立刻满仓撸安达威尔，新年第一个板，感谢A叔每天的无私指点[合十][合十][合十]    

> 星子:   
最佳戏精[偷笑]出货的分时    

> 曹xq:   
汇报师傅：300104，601700上午冲高出了，错过板，感觉也正常。300312上午割肉出了，本身昨天进错了。深刻感觉：情绪条件下，要么看承接低吸龙头或换手打板龙头，要么打头排开盘5~10分内完成，当然是针对要复盘预选股。    

> 玩主任易:   
古人用兵重形与势，本质是对抗。所谓形名，分数，奇正，虚实，皆为主导之势。用大自然规律中悟出的法则与人性的弱点的结合来指导战争中的对抗之势。股市之势包含主导之势与自然之势两种。国家队机构公私暮为主导之势。散户为枢纽之势，两者相交形成形成股市自然规律大势。这是与兵法之势的区别。从大自然法则看，战争也是局部主导之势，国家趋势与自然规律则是枢纽之势，两者相交为大势。从兵法原理看势只有正奇之分。两者相互转化，循环。 从以情绪，资金，政策为主的推动市看，趋势大局观也分为正势，奇势，契合期三种趋势。 正势特征，以政策条件，情绪成势特征，盈亏比超预期为主，支流题材起到助攻的效果。 奇势特征：以正势惯性，新题材，情绪成势特征为主，盈亏比多样性。 契合期特征：本质是奇与正的相互转化。题材多样性，情绪不稳定性，以筹码结构，股性，盈亏比为主，盈亏比典型性受限特征。    


