### **题材的思考**
> 作者:A神asking   发表时间:2020-01-08 19:41:56   原文: [点击跳转](http://mp.weixin.qq.com/s?__biz=MzUyNTM3NDk5MQ==&amp;mid=2247485686&amp;idx=1&amp;sn=92ab58bffacc72c3bcc3621f9f1b4fe1&amp;chksm=fa1e4b96cd69c28058becefa68f2416d82a3729af896bc53d5105c92d1e9495fad26dded2d00&amp;scene=27#wechat_redirect) 

---
一直陆陆续续有新老朋友来，大家也蛮关心A叔的近况,可能大家都知道我的经历，现在当然早过了财务自由的阶段，这两年过着炒炒股票，打打德州，偶尔澳门一日游，腻了就去海上漂几天的日子。
股市没有神，我说的也不尽然全对。公号取名的时忘了告知小友助理，用A叔就好不要用神。所以以后大家留言，客气的话叫我声A哥或A叔就很好。
这里主要分享心得和交流逻辑，股市是一个自我修炼的地方，前辈们也最多给你们一些提醒，让你们少走点弯路，最大的敌人还是你们自己。而你会发现其实最终能帮你的也只有你自己，我不行！养家不行！小赵也不行！
—————博观而约取，厚积而薄发—————
竞价开盘
三大指数同步爆量。
延续昨日尾盘，情绪不佳。

![](./imgs/1585426418.735404.png) 

下跌放量，但题材效应仍强。
估指数弱修复后或继续调整释放获利盘。

指数放量调整
情绪午后回落
题材多线续攻

网红经济博通股份天龙集团二板。
第四轮补涨，板块趋弱。
后续仍有套利机会，但时点要求较高，整体暂做规避。

MODEL3降价&产能爆发预期&国产替代进程预期下，机构强推，特斯拉板块爆发。
天汽模，奥特加，常铝股份三板。
只模塑科技二板。
多股首板。
世运电路放量收阴。
板块中位分化，整体仍有延续。

农业转基因板块龙头大北农换手。
荃银高科强封四板，后排全线调整。
政策加持&行业面变革，板块仍有预期。

山煤国际一字三板引领异质结构电池。
后排梯队仍在，板块仍有套利机会，持续性不明。

昨日推演为：
牛市氛围下，此次演化路径：
量能不济--情绪回落--指数仍强---震荡？调整？
今外围利空叠加下，市场选择调整。

指数调整，部分释放获利盘兑现需求。
明日早盘正常应延续惯性，但赚钱效应爆棚后，暂估调整幅度不会太强。
整体适当收缩仓位，仍可寻找市场G点。

以特斯拉和转基因为例的题材思考：

一：如何理解两个题材的级别？

二：分别做两个板块发散形式&梯队演化的预判。

三：根据的消息面和个股的驱动解读，呼应题材内部结构演化的影响有何不同？

留言，分享。

干货链接点下面蓝字查看


![](./imgs/1585426418.9354818.jpeg) 
何为筹码，一文秒懂

免责：本号所提个股仅为个人复盘所用，不推荐任何个股，不加个人QQ或者微信，亦没有任何QQ群或者微信群，股市有风险，投资需谨慎！！


![](./imgs/1585426419.0777602.png) 


我每天耐心的教，小友们有没有收获？
如果实在看不懂，就慢慢学。不懂又没耐心的话，劝君取关不浪费时间，因为炒短线不一定适合你。
如果有收获，可以点个赞，并帮我推荐转发给短线的同好，一起积累福报，也让我能够得偿回馈市场的心愿。

大家有问题可以踊跃留言
好的留言我基本都会回复

留言区有黄金！


![](./imgs/1585426419.224992.png) 


---
> 愚公移山:   
1.此次一浪突发事件，无疑给国内的主和派（妥协派）的部分人提了醒，在利益面前，没有什么协议是不能撕毁的，所以单纯寄希望与金毛第一阶段的签字然后高枕无忧的心态，要不得。  芯片、粮食（主要为转基因大豆、转基因玉米）这些目前对外依赖度高的东东，要加速国产替代了，芯片已经在大基金的支持下开始了一系列的并购、研发等动作，那么接下来应该轮到粮食自主发力了，2020年预计对转基因的研发和商用推动力度，会超出一般人的预期，特有体制下的高效率会得到超常发挥，粮食安全高于天！  所以转基因的题材最高！ 电动汽车，则是用特斯拉这条鲶鱼让国内低迷的构配件企业获得一次重生的机会，毕竟整车与国外差距太远，所以要用外来的和尚念经，最终还是要让本土的和尚（整车厂）也学会念经。转基因级别高于电动汽车。如果说电动汽车是海外电动趋势下，对国内相关配套产业链（暂不考虑整车）的利好，上游的电池、零部件会有中期行情；那么，国内粮食安全引发的转基因育种、植保、检测等产业链，将是2020年最大的题材！后面不排除在相关部门的推动下，展开系列的并购、合作等整合动作，以加速这一产业的发展，所以继续持股是最好的选择。  2.板块发散方面：转基因有大北农+隆平高科，发散到大北农+荃银高科，今天前者的炸板，后续可能由先正达资产注入预期下的荃银高科继续带队。当然前者远没有走完，估计最多调整半个月到20日，开启第二波炒作，将带动产业链下游的育    
>> 作者:   
[啤酒][啤酒]
 

> 大良造:   
兄好，这是你近期出的最难的题了。题材级别，需要做更多的深度逻辑思考。特斯拉是贸易战妥协的产物，也是我们打开国门向世界证明我们开放的姿态，因特斯拉可平息米国遏制我们的鹰派风头，特斯拉更是我们恢复蓝天的需求，改善尾气结构，向善，融入了我们的很多产品，视为双赢。特斯拉帮我们拉动内需，出口日韩，未来具有战略性，可预见的未来，日韩，德英法在汽车方面，应俯首称臣。战略性战略性！转基因不是过街老鼠，同样是贸易战角逐的另外一个平台，我们的粮食安全必须掌握在我们的手上，特别是玉米这块，大量的工业，药品，饲养都要用到玉米淀粉，我们过去进口人家的，遭遇卡脖子不是闹着玩的，如芒刺在背，因此须解决！高层高瞻远瞩，俯瞰寰球，必须清醒认识此问题。机构不傻，狂顶狂买。两题材都是实实在在的预期摆到那儿，读书人自己应思考背后事件。至于网红那摊事，别入戏太深，因星期六抵抗减持而走妖，视为刚烈，正好赶上大盘指数回升周期，振臂一呼，群雄响应。后进出时点越来越严格了。每个题材出现时的指数节点，情绪节点，共振最佳。胡言乱语，诸君一笑。😂😂😂    
>> 作者:   
可[啤酒][啤酒][啤酒]另网红是初生产业，不应低估拓展的空间。
 

> 日出日落:   
一：如何理解两个题材的级别？ 个人感觉特斯拉应该属于中线题材，接下来会有几波炒作，现在算是预热的第一阶段，受困于短期产能限制，很难很快反应到供应商业绩，所以第二阶段可能就是业绩预期释放时和第三阶段销售超预期还会不断的炒，中间会夹着国产化零部件新消息局部炒;转基因属于短期概念题材，要批准加培育种子等，至少需要两年后能体现在业绩上，感觉短期内一波流; 二：分别做两个板块发散形式&amp;梯队演化的预判。 特斯拉板块是题材发酵高潮后，不断高位分化+低位挖掘补涨，需要板块梯队支撑龙头往前走;而转基因实质正宗标的太少，就是缩荣炒作，作为次主流，不需要梯队支撑，龙头拓高带动补涨个股，可能也有新的挖掘，类似大麻; 三：根据的消息面和个股的驱动解读，呼应题材内部结构演化的影响有何不同？ 没有完全理解这个问题的意思，感觉特斯拉伴随着题材消息面或个股的相关利好，都会刺激板块的强度和高度以及龙头股高度，而转基因更类似于独立股炒作，龙头打出高度，后面有消息的股补涨，对整体影响有限。不知这个回复是否答非所问。[呲牙]    
>> 作者:   
一特斯拉可。二可，但次主流歧议。三确实问题比较难表述，主要是个股的深度解读和盘面资金呼应的对照解读。
 

> 遇见美好:   
1、转基因与特斯拉发生在金融、网红之后，机构效应在转基因上得到加强，但是总体上的级别和高度应该是低于网红，转基因是种业行业发生变革，特斯拉属于新能车大政策下的样板工程。转基因高度应该大于特斯拉，而特斯拉的时间宽度应该是大于转基因。 2、转基因属于龙头强，从而带动跟风。不仅发散到种业，还有除草剂农药等，同样的，也是跟风先跌，最后到龙头分歧。特斯拉属于进展超市场预期，出消息的时候起步阶段大家基本同一水平线上，属于连板晋级成王败寇模式。但核心受益股和业绩弹性大的也有可能走出趋势。 请A叔指点[啤酒][啤酒]    
>> 作者:   
可，有一个像样的了[啤酒][啤酒]可继续拓展和深度。
 

> 清欢:   
个人预判新周期的龙头是奥特佳。    
>> 作者:   
炒股不靠梦想[呲牙]
 

> 向上:   
转基因几个大品种爆量，修复难度过大，题材再起的概率偏小，网红概念应该还有反抽预期，引力传媒和星期六筹码未散，明天应有低吸点。特斯拉产生链逻辑强，且大多为小市值品种，估持续性会更好    
>> 作者:   
有量时，流动性才是大资金追逐目标，如东方通信，顺灏股份，中国软件等主升阶段。
 

> 执念:   
一、起初理解，特斯拉和转基因为网红经济赚钱效应下的补涨题材，但今日特斯拉显强，特斯拉至少是与网红经济同级别的主流题材，转基因题材还是不看好。 二、（1）转基因是在网红分歧之时，大北农封单转强、隆平高科一字作为引导，未来若要走强，须由大北农继续超预期带动。（2）特斯拉由机构强推，全面起爆后进入淘汰赛，未来决出龙头后，沿着龙头属性进行发散，低位不断挖掘新分支进行补涨，高位龙头震荡或继续向上。 三、（1）转基因是头部受益明显，由头部个股上涨带动后排进行补涨，未来没有消息预期。（2）特斯拉昨日上新闻联播，未来应不断有消息出来，如某公司进入特斯拉产业链进行刺激，100%国产化受益公司较多，市场将挖掘个股进行补涨，从而推动龙头股在高位震荡或继续向上，重点应在稀缺厂家，如某部件在中国只有一家能做。  操作上，回封扫了奥特佳，先分歧反而是优势。    
>> 作者:   
一新老周期无缝衔接的概率大些。  二特斯拉不存在总龙头，中长期走业绩释放预期，唯一可能出准龙是板块内某股被其他属性强加持。  三转基因本月20日公示截止日是明确的落地时点。特斯拉供应商已是明牌，未来供应商调整才有预期差。
 

> 大黑马:   
A叔，国际形势如此的持强凌弱，军工不出阶段起势吗？    
>> 作者:   
当时因确定性建国大庆可搞军工，平时偏爱蛋在人手，净值曲线不稳。
 

> 常德强:   
师傅说后手稳健，我一直默念，14.20出手世运电路，直线拉升，额滴神啊！然后，就没有然后了。是概率问题吗？    
>> 作者:   
是瞎买，午后冲高为卖点。题材看不清，答案就在今天的问题里。
 

> ...:   
A叔，想请教下，您说之前锐明技术估值低估，目前已到合理区间，那么这个合理区间是对比整体板块的估值还是？    
>> 作者:   
是独立个股，纯基本面。
 

> 白:   
A树，小弟感觉转基因才是大题材啊。因为锂电池、氢能源、新能源车炒了也不是一天二天了，这次只是因为特斯拉往事重提而已。反观转基因，一方面能解决我们的粮食+猪肉问题，不受制于米国，二能解决农民的收入问题，实在属于国家战略题材，所以仓位都去了转基因，结果大亏。    
>> 作者:   
是节奏把握问题，不是题材级别预判问题。
 

> 小王子:   
A叔，晚上好啊。 1.特斯拉题材第一天高潮，然后就分化，然后就出现亏钱效应了。题材性质一般。高度三板世运电路凉凉。 2.种子高度八板尾盘强封，大佬云集看多。散户看空。预期差还是有的。明天看多做多。 3.日出东方叔看不上吗。今天这么亮瞎眼。。难道也是英雄末路？    
>> 作者:   
三走博弈，逻辑尚不如另一个东方。
 

> 刘建华555:   
今天的消息面，伊郎发射了导弹打击美军基地，盘面军工走强，盘中伊朗又说结束战斗，盘面军工开板，后又回封，    
>> 作者:   
都要给国内交代，都要演[啤酒]
 

> 得与失:   
师父，我发现节奏与分寸问题好复杂，上牵扯预期，下对应感知，辅助以动态预期修正，节奏里还对应复盘选股侧重点与风险规避，如常见的获利盘兑现节点情绪回落与量能回落节点，甚至追高预判低吸炸板概率预判，还有要命的买点时间推后求稳不占先手，最后这一点通常什么情况下要推后，侧重点是啥，师父完善点一点?    
>> 作者:   
因冲高反杀的可能故后手。重点是根据动态盘面，推演个股明日预期。
 

> 日出日落:   
A兄提到的特斯拉很难出准龙除非有其他强属性加持，您思考的原因是特斯拉概念本身的业绩比较容易估算（产能和销量，简单一算门清），不够模糊，弹性不大，想象空间不够的原因吗？还是因为题材板块内概念个股很容易挖掘光，这样持续的时间上就很难拉长，这种靠板块支撑的龙头，很难打出高度成为总龙头。[抱拳]    
>> 作者:   
两者都有[啤酒]
 

> 向上:   
今天锐明技术五点多出了，看龙虎榜应该是还有机会，上午二封仓控上了旭升科技，下午三个点低吸风华高科，平盘低吸引力传媒，云南锗业，拿先手。    
>> 作者:   
仓控可试[啤酒]
 

> 十年:   
1.特斯拉存在于新能源大主线内的亮点题材，应是长线题材，但筹码最复杂故走出趋势慢牛行情。转基因属于突发性题材可以看到2385.0998.300087都走出天量筹码。存在于贸易战下行业质变的题材，有15号签订农业进口和20号批准和两化国企改革题材写不下了老师指教  2    
>> 作者:   
是[啤酒]
 

> 铷妍:   
1.特斯拉是影响传统出行的变革，随着新能源汽车份额的不断扩大，产业链的机会是以年记的。所以有机构把特斯拉产业链与苹果产业链作对比。转基因也算是消费领域的选择扩大，我国是农产品消费大国，在加上每年一季度都要炒1号文件。所以转基因在一季度应该是在风口上。  2，第二是啥，我看下题目再想想啊    
>> 作者:   
[呲牙]
 

> 💏陸研霖🍭💋:   
一.特斯拉应为趋势行情，转基因为快进快出行情二.特斯拉为科技分支可发散锂电充电桩，卫星导航甚至5g，各小分支形成t队.三.我连题目都看不太懂，看大家分享[捂脸][捂脸]叔，今天指数下后排套利不去，尾盘大北农套利可否？就是t队都没打的不自信    
>> 作者:   
可去
 

> 阮泽蔚:   
第三阶段：是业绩证伪的兑现期  这个时候基本面的研究就很重要了，你弄一个贴边的概念出来扯淡那肯定是不行的，市场也不傻。比如新能源汽车，第一个进入业绩证伪的板块就是关键原材料：锂。（因为业绩爆发了）  这个阶段，就好比清华毕业的学生真的进入职场了。这时候就开始真正非常功利的衡量你的成就了。说虚的没用，就是看你的业绩，你的生产能力。（你的市场占有率，你的产能等等等等）  而往往第三阶段的炒作，是以趋势为主，短线上表现并不强势（连板等），但恐怖在月线级别的主升浪。这个阶段，最核心的，就是对于基本面踏实的研究。（有点接近价值投机了）  板块性业绩大爆发+一个触发点（特斯拉国产化进度和速度高度的舆论关注度）=月线级别炒作启动，炒很久很久。    
>> 作者:   
[啤酒][啤酒]
 

> Guo:   
想了想，自己先做个作业再看评论。  转基因：国家政策推动下的行业利好。 特斯拉：行业企业模式爆发引起深刻变革的利好。 都是传统行业，虽然利好原因不同，都是革命性变化，个人判断影响力级别上差不多，但是车离普通百姓更近，关注度更高。这样看影响程度特斯拉更大。   板块发散过程： —转基因政策公布后直接利好大北农，时间爆发纯正。后排利好程度逐步减弱，龙头是强中之强。龙头倒板块倒。 —特斯拉第一阶段起爆为12月上旬工厂宣布即将开售。本次爆发为周末公布杀手锏29.9万售价时震撼整个汽车行业和券商研究员。但产业链板块已开始动作，世运电路周一已经二板领先。即龙头并非最正宗收益股，今天倒了并不会阻止真正受益的千百家上下游产业链供应商的脚步，且随着产量销量的逐步提升会越来越强，无论短线还是趋势估值。今天顺大盘情绪及外围环境完成新老接替正好。给纯正标的让路，一切刚刚开始。  暂时想到这么多，感谢A兄的作业，这是很高级的思维训练，正本清源，指明方向，做中学。跟各位学习思虑不周之处。    
>> 作者:   
一特斯拉解读有误。  二纯正标众多，且估值逻辑下机构众多，偏离大会被砸。
 

> 阮泽蔚:   
第二阶段：是初步投产的试水期  这个时候主要是看政策面的支持和铺开程度，一般会有一个大的爆发都是在这种背景下。比如当初最牛逼的，上海自贸区。政策公布之前，实际上反应一般，仅仅是上海物贸等零星票炒作，但是等政策真的落地了，一下子就炸了。为啥？因为这个题材偏新，大家并不敢期待，只有真正落地了，才开始相信真实性。雄安新区也是如此，实际上这个新区早在几个月前就已经有传言了，但市场没有任何反应。直到公布。  这个阶段有两种题材最容易被炒作： 1.政策性变动。（自贸区，雄安等） 2.本来就具备未来想象力的。（人工智能，新能源汽车等）  超级大的想象力+加上看得见的实质性进展=当下要爆炒，炒很久很久。    
>> 作者:   
是路径之一[啤酒]
 

> Cuysding:   
叔，关于题材铷妍姐和大家都说的比较透彻了，叔的点评很值得回味。    
>> 作者:   
遗漏的重要细节很多，如没人想到，也不想深入了。
 

> 龐:   
叔好♥，不是龙头不倒就可以后排套利吗，为什么转基因不是这个逻辑呢，为什么大北农前天还一字时后排首板都没了    
>> 作者:   
题材本身分歧就大，龙头断板日预期下抢跑正常。且跟风大都无业绩实锤兑现预期。
 

> 衍生:   
A师，转基因属于政策性驱使，长期利好相关企业，这里先不讨论转基因对错好坏。特斯拉全部国产替代，且国家强推新能源车，相关企业长期利好，两个题材相关企业特斯拉能更快的见到效益，相关题材短期强度强于转基因。 今日世运5个点出局，昨日没做计划内的天汽模，做了世运高度，反而被卡，A师，这样的情况，是不是可以预判一下，特斯拉第一波不会高过4板？我是看好二波的    
>> 作者:   
业绩预期的波段趋势，非特殊情况下，新鲜度褪色通常二波难超一波。
 

> AWP:   
一：如何理解两个题材的级别？  转基因发酵于情绪亢奋后回落初期，时间节点好，特斯拉是发酵于情绪高潮期，故表现为全面开花，但高度板空间不够，我理解转基因级别较大，因为龙头打出了空间  二：分别做两个板块发散形式&amp;梯队演化的预判。  转基因高度一字板牵引，登海为换手龙，荃银高科为卡位龙，后排补涨，然后无首版补涨—换手龙及连板股不及预期掉队—总龙头尾盘开板，后续演化为总龙横盘震荡或反抽见顶或二波，然后属性发散后排补涨。  特斯拉演化为全面开花，总龙正好卡住高度—淘汰赛—总龙澄清，掉队，后续演化应为板块内轮动补涨，高度在2板内概率较大  三：根据的消息面和个股的驱动解读，呼应题材内部结构演化的影响有何不同？ 转基因为具体品种获批，龙头应是最直接拥有获批产品的票，而特斯拉为产业链变革，演化顺序为产业链中率先受益的票及利润较高有业绩预期改变的票，从而扩散到全链条，离核心越远高度越有限    
>> 作者:   
一级别大才有龙头的空间，因果反了。二特斯拉轮动二板上限定论太早。三大致可[啤酒][啤酒]
 

> wang.v-58 ₂₀₂₀ོღ⁶⁶⁶:   
特斯拉涉及的行业广，从化工、汽车到科技行业，收益范围大。转基因收益的群众数量多。两个都是大题材。二、题材演化，特斯拉今天推演到充电桩，可能再推演到国内的整车、电改等，对原油是个小利空。转基因的题材演化，可能后续的化肥、农机等乡村振兴，题材延续性不好，除非有持续利好。三特斯拉已经炒作两波，成交量突破不了，利好可能也是反抽，农业尽管持续性不好，但洗出老筹码，再来利好可能二波的机会会很大。请A师点评    
>> 作者:   
一可。二三无法点评。
 

> TiAmo:   
叔，大级别的题材有广度有高度，转基因启动的时间点比较好，和大盘共振，也有资金做盘。转基因有想象力有高度却没广度，赚钱效应在板块启动时及前排个股，板块也不具备大的发酵空间。个人觉得题材的级别取决于消息、大盘的共振程度、赚钱效应。大级别的题材让大多数资金吃到肉，会吸引更多的资金入场博弈，高位才有二波三波，低位发酵补涨。请叔指教 1.今天仓控水下搞了世运电路，原因前半个小时承接好，下杀也未破早盘分时低点，特斯拉、OLED还有预期，盘中富贵 2.日出东方看图形选股，丰乐种业、深大通都是这种图形，板上小仓试手拿先手，叔，这种水下拉红半路是否也是买点？    
>> 作者:   
一先手持仓的卖点。二可放弃。
 

> 绛紫，虾米:   
叔，今日操作：均线略上卖世运，并未出在高点。锁天汽模。入大北农。可打几分？问题在哪？谢谢叔。    
>> 作者:   
75
 

> 镰刀（阳阳）:   
叔晚上好！ 1特斯拉可能是未来的爆款，国产替代长期利好，特斯拉潜伏盘较多，有的已经长不少了，比如宁德时代，没有只涨不跌的股。转基因是改变一个行业，是个长期题材，缺点是个股盘子大，个股少不形成板块效应。 后两个问题在想想[奋斗]    

> 曹xq:   
题材启动日板块联动多的好，其后梯队完整的好，题材发散的好，有换手高度龙头的好，配合指数情绪冰点后转折的好，    

> 灵狐:   
特斯拉和转基因均属于行业大变革，题材级别类似于工业大麻和折叠屏，但由于题材节点未能卡到指数和情绪转势点，高度应不如。转基因个股较少，正宗的概念因盘子过大今止步，但可能调整数日后用荃银高科作星星之火起燎原二波。特斯拉概念众多，但世运电路止步，龙头高度应不高而是小龙轮动可能性大。 今天操作早盘世运电路换手过大，在板块效应较好情况下不板出了，错过下午的高点。后打了荃银高科回封，没想到午后筹码松动还能强封，早盘打板长城军工回封加仓。次新科安达和指南针小套拿着，指望三字名就成妖太不靠谱🙈。    
>> 作者:   
[啤酒]
 

> 愚公移山:   
接上条。将带动产业链下游的育种、植保、检测等产业链开始板块联动，比如草地贪夜蛾和草甘膦。后者的发散形式，标的太多，核心的也有10多个，目前还处于市占率空间提高下的预期炒作，所以弹性大的走出加速，后续走中线的逻辑较大，核心标的就是宁德时代，旭升股份和拓普集团等，看好国内有一定优势的锂电池方面。包含钴锂矿等。特斯拉的超短行情，成为了竞价排单的淘汰赛，介入难度较大。  3.内部结构的演化方面，不是很理解，是否就是电动车存在标的之间的补涨和助攻，而转基因目前标的太少，龙头一字板，挣钱效应差，所以后排小弟难以翻身的意思。  请A叔指正。    
>> 作者:   
也不盲目高估，根据盘面做修正[啤酒]
 

> 夜白zZ:   
感谢A叔的谆谆教导，以下是我的思考: 一：如何理解两个题材的级别？ 新能源车是国情趋势，引进特想着斯拉是促进国内新能源车公司的产业升级，不仅仅是靠国家政策补贴来盈利。新能源车市场从政策到预期都是实打实的存在，这中间有逐渐释放的过程。 转基因玉米目前发放证书，但是产业化过程中需要谨慎推进，转基因产品还是有些存疑。相比较特斯拉＞转基因。  二：分别做两个板块发散形式&amp;梯队演化的预判。 两个板块发散形式不同，新能源汽车爆发是面状爆发，周一多只股涨停，逐渐晋级，在晋级中逐渐出现板块龙头。转基因是大北农率先涨停后，带动板块效应。板块个股需要看大北农脸色形式。  三：根据的消息面和个股的驱动解读，呼应题材内部结构演化的影响有何不同？ 新能源汽车中的个股，偏向于普涨或者轮动，比如新能源车零配件供应商、锂电池等。转基因板块中的个股大北农龙头倒下，那么这个板块就退潮了。    

> Karen:   
1、特斯拉题材发散效应强，可促发相关产业链，有资金流动性，进一步分化。2、转基因题材符合当前农业发展需求，关乎民生问题。板块较小，但可期待二波。    

> 阮泽蔚:   
叔好。我来大概量化一下，一个超级题材炒作通常分为三个阶段。  第一阶段：是概念初次提出的朦胧期这个时期不用太多关注基本面的东西，基本上沾边的票都能炒，并不需要太强的基本面，主要还是看各种机缘巧合的市场选择，大多时候是随机的。这个阶段关键看这个概念的可接受程度大不大。靠谱不靠谱。这个阶段，博弈成分最强。因为不靠谱，大家没有信仰，只能跟着盘口来。谁是龙头，谁是跟风只能市场说了算。没法证伪。随机性太强。    

> 陈应军:   
特斯拉是长期的大题材，现在短线的板块梯队好。发散先直接到间接供应商，然后可以向电池、充电装等新能源方向。也可以出东方通信相似的大牛。转基因爆发性题材，但板块梯度差，暂时赚钱效应差，个股量大，可能要调整后再起二波，发散方向农业、猪肉。老师指正，谢谢！    

> 得与失:   
市场兑现预期下，暂避高位。目标锁定在低位题材与补涨上。以特斯拉的持续性，预判会有低位晋级三板和首板挖掘，实盘资金顶天汽模与奥特佳卡位，今日整体买点推后下无盈亏买点。结合整体市场，众业达虽承接不错也无吸的必要。均胜电子半路或打板做首板预期，可有盈亏，得分多少？低位太阳能山煤牵引下，若无市场兑现预期的推后买点，开盘买东方日升可多少分，今日买有多少分?想跟师父确认下预判与买点锁定    
>> 作者:   
70。70。是控住总仓位后的得分。
 

> &:＆:   
一个是农业自足保命调解通胀 一个是工业化升级智能化变阁包涵上下游产业链 前者更多是政策驱动，后者更多是业绩预期驱动 前者公司集中后期可能受益强者恒强的头部公司，后者更多要在不断优胜劣汰中胜出，还有可能会出现新的颠覆者，多仰仗于市场调解和资金风投 前者炒作一步到位，挖掘空间寥寥，后者上可到材料机械，下可有整车配套后期维修充电，挖掘可随着消息刺激小波接一波 明日更看好特斯拉，应应有进级和反包个股    
>> 作者:   
前半部分可[啤酒]
 

> 昱安:   
转基因（农业）＜特斯拉（科技），A股那么多行业唯有大多数科技能跑赢大盘，传统行业家电服装酒饮料等，大多跑输，除了顶部领头公司。 转基因种业就目前来看是个过度，如果拿网红经济，更好的称呼应该是传媒经济，个人影响力，传媒平台与商品交易相结合，有别于传统商品交易。当作暂时的一个大周期的话，种业崛起时星期六处于分歧，大盘环境也好。类似，种业高潮时，特斯拉自威唐倒后又崛起了。后面的高度看天汽模与奥特佳。奥特佳叠加军工，换手充分，量价齐增更看好。天汽模还是沾了公告的光。但高度应该还是受压制。 板块内都是确认，发酵，后排助攻，然后分化，不断有挖掘出来的补涨，也有跟不上的掉队。最后高潮落幕，散户几家欢笑几家愁…市场残酷。 可惜了煤炭，蹭不到特斯拉，蹭不到转基因，好不容易蹭到山煤国际，想不到今天第一个被杀祭旗…成了一日游。重仓安源煤业（亏2.48%），半月来第一次日收益为负，短期死在自己低位挖掘模式中。 后期星期六的2波中，5G在路上。那才是史诗级的题材。各路中小级别题材软件芯片，屏幕等等助攻，权重，周期轮张，A股走向慢牛。 总体而言，对于超短选手机会始终有，在刀尖舞蹈，要有敏锐的触觉与高度的知行合一。    
>> 作者:   
😂😂
 


