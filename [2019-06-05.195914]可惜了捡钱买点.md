### **可惜了捡钱买点**
> 作者:A神asking   发表时间:2019-06-05 19:59:14   原文: [点击跳转](http://mp.weixin.qq.com/s?__biz=MzUyNTM3NDk5MQ==&amp;mid=2247484983&amp;idx=1&amp;sn=dc0637a0105a318bf8d0b4f81dfea767&amp;chksm=fa1e4557cd69cc41af2c194885bc60153d3d6467e412e3e86d2c065001fabbd108d4ed4a54d6&amp;scene=27#wechat_redirect) 

---
一直陆陆续续有新老朋友来，大家也蛮关心A叔的近况,可能大家都知道我的经历，现在当然早过了财务自由的阶段，这两年过着炒炒股票，打打德州，偶尔澳门一日游，腻了就去海上漂几天的日子。
股市没有神，我说的也不尽然全对。公号取名的时忘了告知小友助理，用A叔就好不要用神。所以以后大家留言，客气的话叫我声A哥或A叔就很好。
这里主要分享心得和交流逻辑，股市是一个自我修炼的地方，前辈们也最多给你们一些提醒，让你们少走点弯路，最大的敌人还是你们自己。而你会发现其实最终能帮你的也只有你自己，我不行！养家不行！小赵也不行！
—————博观而约取，厚积而薄发—————
开盘
沪指略放量，深指放量，创业板爆量。
竞价情绪极好。
昨日说的反弹预期一步到位，因为是全球降息预期激发，非内生动力，需持续观察，以确认反弹强度。
因创业板量能充沛，估题材会有表现。

实盘看：开盘第一个15分钟量能急剧萎缩，半小时量已同比弱于昨日。
修正预期：反弹强度调低。
收盘看：开盘虽爆量，全天量能继续萎缩，该强不强是为弱。

指数无量回落
情绪冰点反抽
题材多线轮动

会议激发稀土开盘一致，国泰集团领涨三板。
金力永磁受板块激励冲高，筹码结构决定不能延续强势。
砸到跌停后，借残留人气反抽翻红，尾盘回归深水。
总龙继昨日冲板，今日反抽继续消耗人气，短期失去反包动力。
如非昨日瞎搞，今日是必板的确定性换手捡钱买点。

铭普光磁无惧减持，六板领涨5G，中兴通讯中阳，板块今日最强。盘后人民日报公众号站台5G元年。

昨日丰乐种业带崩农业，今日板块错杀修复。

华为概念多股涨停。

芯片板块多股涨停。

涨停共性为多题材叠加。
最强叠加属性次新，送转次之。

前热点全面轮动，且不见量，疑似毕业合影。
临变盘节点，做两手准备。
如反转向上，需量能辅助，近端次新为最佳突破方向。

端午前最后一天，总体忍手。
近端次新或新题材或有卡位试错机会。

免责：本号所提个股仅为个人复盘所用，不推荐任何个股，不加个人QQ或者微信，亦没有任何QQ群或者微信群，股市有风险，投资需谨慎！！

![](./imgs/1585426918.9810488.png) 

我每天耐心的教，小友们有没有收获？
如果实在看不懂，就慢慢学。不懂又没耐心的话，劝君取关不浪费时间，因为炒短线不一定适合你。
如果有收获，可以点个赞，并帮我推荐转发给短线的同好，一起积累福报，也让我能够得偿回馈市场的心愿。

大家有问题可以踊跃留言
好的留言我基本都会回复

留言区有黄金！

![](./imgs/1585426919.1186788.png) 


---
> 李四青:   
A叔，今天金力永磁的走势彻底打脸昨天那个叫嚣10.30涨停的那个评论。不过A叔昨天说金力永磁筹码不好，没有沉淀，是不是因为昨天没有封死涨停啊，所以很难有第二波。能指点一下么    
>> 作者:   
昨天如果午后沉一沉再洗一波，持股的筹码就稳定了，估计今天板了，明天还有连板预期。置顶的那个也无所谓打谁脸，只是幸苦回复的时候，看到这种留言并不好受[啤酒][啤酒]
 

> LI:   
排单成交铭普光磁，a叔打几分[偷笑]    
>> 作者:   
不及格
 

> 小辉:   
A叔，昨天首板的朗科智能和武汉中商差不多是接近涨停开盘,两只股开盘都有秒板的可能。9点30后朗科智能秒板了,打板基本是打不到；武汉中商却高开低走。 两只目标票在竞价9点24分钟都是逐渐放量的。如果想买朗科智能这种秒板股,在竞价9点24分钟时下单涨停价是可以成交的。那么请您指教一下,有什么量化数据在竞价最后一分钟时可以预判朗科开盘会秒板,武汉中商会高开低走,来帮助下单选择正确秒板的朗科。    
>> 作者:   
流通市值差很多，抛压自然不同。非核心板块，情绪下降大周期，赌一字输了是活该，对了是侥幸，后果更坏🙏🙏
 

> 花花:   
师傅近端次新哪个题材稍微好点？比如惠诚环保芙蓉科技中简科技等等    
>> 作者:   
近端题材是辅助，更重博弈，中简占优。
 

> ...:   
A叔，感觉这次明普光次有点像科创时候的超华科技，都是情绪杀了之后的一个反弹标。    
>> 作者:   
次新叠高送叠磁，但有减持，不能入戏深。
 

> 感激生命:   
叔，今天看到泉峰汽车继续上板，低吸了点日丰股份，日丰股份有反包预期么    
>> 作者:   
跌停买点
 

> 🔥:   
今天国泰吓跑了[捂脸]，功力不足！    
>> 作者:   
板砸没问题[啤酒][啤酒]
 

> aliebo:   
a大，铭普光磁不怕这个减持公告，可以理解为庄游格力板吗？这个庄而且大概率是公司派的庄，要不然这么明显的利空，还有资金去封板？我认为是公司和柚子在某种意义上达成了一致。请指教[抱拳][抱拳][抱拳]    
>> 作者:   
嗯，四板分时正常放量是55%+，实际只有38%。
 

> 不忘初心:   
A．	盘前预判： 指数周期：连跌4日，指标再次临近半年线，预判至少会有小反弹 情绪周期：昨日涨跌停比为36比60，赚钱效应极低；预判今日有回升； 题材周期：大周期还在稀土内。5G队形较完整，铭普光磁4板带4个2板；稀土概念龙头高位振荡，补涨龙国泰集团2板带3个1板；次新崩溃未修复，远离； 总体：今天有科创抽血，但就5G/稀土比较，稀土晚间有利好加持，补涨龙处于低位，个人更倾向于这个概念；但一切要以盘面说了算；  B．竞价：大盘量能略高于昨日；稀土高开较多，国泰/宇晶一字；5G高开幅度低，无连扳；量能而言，却是5G高于稀土(直接无视，好尴尬)；最终决定搞稀土。  C．策略：5成仓位排板国泰集团，2.5成排板宇晶；结果都排到了；昨天的新余国科，等到10日线后再根据反弹力度割肉，尾盘看有拉升，故暂未卖出；  盘中看着稀土/5G此消彼长，而且资金跑向5G的多于稀土，最终5G胜出，我瑟瑟发抖地挺到尾盘；不知明天能否活着出来。  D．明日预判：大盘今日高开低走，反弹无量，预判明日不会好，而且5G概念胜出。明日找机会开盘出掉手头个股，腾出仓位继续空仓，如龙头有崩溃低吸机会仓控小试；  A哥，我是不是差的太远？麻烦A哥批评.[流泪]    
>> 作者:   
5G胜过稀土，和两个板块的龙头表现有关[啤酒]
 

> 海的传说:   
涨，可以，跌，奉陪，割，妄想！！！希望某些机构认清形势，让大盘回到正常的轨道上来。中国散户不想跌，不愿跌，但也绝不怕跌！如果有人故意砸盘，我们将奉陪到底，经历过5000多点风风雨雨的中国散户什么样的阵势没见过，没什么大不了的。中国散户必将坚定信心，迎难而上，化危为机，炒出一片新天地[白眼]    
>> 作者:   
😂😂
 

> 肖:   
早上看承接有量低吸空港股份，幻想有大长腿，收盘被埋。为什么？麻烦指点下    
>> 作者:   
已经反抽两轮，人气早失。
 

> buffros.李:   
学到了，竞价情绪及量完美，盘中却缩量，却多路径高潮，毕业照！！！！可以了金力永磁昨日板上卖出一半，今日看国泰，银河磁体，故金力永磁底仓没卖出！是否今日金力永磁开盘在水下或者昨日均价线换手更加完美？    
>> 作者:   
今开盘本就在水下。昨日水下后，今天正常小阳开后推板。
 

> 张莹瑞:   
A哥好！今日操作昨天没有来得及卖出跃岭股份，今天拉起清仓，昨日跌停附近低吸泉峰汽车两笔，今天开盘和拉起破线卖出，低吸半路惠城环保各一笔，盘中发现惠城依然是跟随泉峰走势。 请教A哥泉峰为什么会走的更好，是经历过大阴线下杀恐慌盘的原因吗，我理解的是惠城环保套牢盘少而且拉板新高应该更轻松，但是盘面还是低于预期我想到的就是泉峰有价格优势且惠城环保虽然蹭稀土热点有前排一字牵引但地位太靠后，请A哥不吝赐教感谢！    
>> 作者:   
昨日打出人气，惠城受金力影响。
 

> 镰刀（阳阳）:   
叔晚上好！今天中简科技做了个T，低吸了点紫光国微，我觉得芯片如果有新闻配合还能起来，您觉得呢？仓位不到三层    
>> 作者:   
芯片稀土5G都是波段机会
 

> 风轻云淡:   
老师，“半小时量已同比弱于昨日”，如何知道昨天半小时量？    
>> 作者:   
软件都有时间周期的分时
 

> 易:   
这两天稀土行业一直有消息释放，是不是稀土还会有第二波？    
>> 作者:   
看政策和发酵力度，暂不好估
 

> 灵狐:   
铭普光磁和泉峰汽车没入叔的表，看来两个都还不够靓[吐舌] 1.A叔，今天预期的反弹算失败了吧，所以预估还要杀高度和杀情绪？ 2.泉峰汽车竞价涨幅0是否为超预期买点？我估-3左右。    
>> 作者:   
指数上看是，赚钱效应看不算失败。所以昨天午盘说预判潜伏卡反弹的盈亏最高。
 

> 修心见性:   
A哥好：盘面别扭，空仓忍手。感谢哥无私的施法布道，功德无量。不要与不知感恩的人计较，原谅他吧，那是个可怜的人。再次感谢哥，近乎于喂的耳提面命般教导。    
>> 作者:   
🙏🙏
 

> Mr.AleX:   
A哥好，提前祝端午节快乐，一直在运用哥传授的技法，希望能探索到无限接近正确的买卖点，知道路长只能慢慢磨： 1、竞价高开高走情绪稳定，放量高开，但量能更不上有讨砸可能，次新不及预期，5g略高于预期，稀土高开，农业翘板轮动，开盘观察确认主线。 2、操作理解，如做连贯切换要求就会应接不暇，需减少目标： a、华脉科技sb战法，能回封可能是想留军工活口。恒铭达有预期导致卖点犹豫卖到了情绪次低点，心态的蝴蝶效应下错过欣天科技弱转强，再反应过来的1进2功课标民德电子也犹豫后撤单。 b、确认不走次新，日丰股份找卖点，后面的次新逻辑票也就不会去了。 c、两年新股+高送类+元器件类批量发散，助攻铭普光磁上板，题材无新鲜，指数回落，国泰集团补量回封犹豫放弃，万向德农大长腿没有企稳指数（但是情绪点了下），德生科技符合推演仓控扫板套利（比较有把握的时机和逻辑点，是否可以直接跟随盘口半路？） d、午开盘对冲贸易战工具箱消息发散，华林证券借势上板，指数量能不够熄火，富春股份、华星创业、科信技术助攻5g，汇源通信锦上添花的时机点仓控半路跟随套利（预判维稳+强承接+筹码优势），买入后意识到老题材+指数未放量下就是赌了，烂板成必然。    
>> 作者:   
B次新预判还有机会，实盘做跟随。C2908半路打板盈亏差不多。
 

> 李韬:   
有个大胆的想法A叔带大家做空美股吧[呲牙][呲牙]    
>> 作者:   
不会玩的，去被收割么😂😂
 

> liuzebin1986:   
A叔，都说资金如水，往阻力最小的方向流动，如果大盘向上，且量能配合，为何近端次新是做好的选择？是因为近次的历史遗留问题少吗？筹码好控制？还是今天全峰汽车已经做出了表率？可是日丰还在拖累近次的情绪啊。还有，如果有新题材的话，我yy跟这次大大访鹅说的话有关。您怎么看？    
>> 作者:   
筹码干净。两条线都肯可能。
 

> y:   
缩量互掏口袋，大家都难，A哥多担待。[抱拳]现在算难做吗？天地板，地天板，感觉情绪都错乱了。    
>> 作者:   
难度不算高，比去年下半年无量时好做[啤酒]
 

> 彺水:   
A叔好！隔夜外围提供了反弹土壤，竞价高开，开盘后量能不足情况下预估低走老套路。稀土消息刺激国泰、宇晶等一字，但金力依旧低开3个点，没有超预期。 操作请教：1.泉峰预估该-3以下开，今天是否超预期？2.竞价游戏仓300127，这个仓可开可不开，以后还是不开了，意义不大，成功了赚不了失败了还影响心情，盈亏差； 3.欣天个人理解还是筹码好+退潮期博弈环境给了弱转强的机会，迅速拉升半路加仓，但午前京天利撤单炸板没看懂，算送的买点？ 4.国泰开板回封还是控仓了一笔，这笔操作有些纠结，复盘就想到如果今日连板很可能被澄清，但人气指数也确实高； 5.泉峰震荡换手承接试错买点？ 6.提前祝A叔端午佳节愉快！[啤酒] [啤酒][啤酒]    
>> 作者:   
一超了。二量不够。三不。五换手足后可仓控试[啤酒][啤酒]
 

> 龙:   
为什么说昨天丰乐带队？    
>> 作者:   
带队跌停😂
 

> 郭军平:   
今日早盘稀土貌似很强，两个一字板。但是考虑到所有稀土都是金立的补涨，金立昨日拉高过早，消耗了人气，今日反包概率降低，进而对稀土板块带来压力。5G有未兑现利好预期，盘中两个二板换手上板助攻，其他小弟也在高位徘徊，判断今日5G强于稀土。下午资金终于做出选择，转而大力攻击5G，金立及稀土被抛弃。这个思路对不对？    
>> 作者:   
稀土整体不弱
 

> jesse:   
A师您好，看到您几次预估爆量换手，有的是一字板的有的是换手板的，除了前平台最大量外还要参考什么来估呢？感谢解惑[抱拳][抱拳]    
>> 作者:   
历史最大量，图形上股价位置，股东结构。
 

> lucky:   
A叔晚上好[啤酒][啤酒][啤酒] 今天深刻体会到：有术无道一场空。特别是术还不精的时候[捂脸]午后介入力源信息 收盘后思考了一下，午后5G再次异动，当时有三个标的，汇源通信、力源信息、恒铭达，最后成功上板的是汇源通信。一个很重要的原因就是：承接。汇源承接几乎都在水上，力源及恒铭达大部分在水下承接，恒铭达最后走得最弱，因为开盘后它的承接最差，直线下杀接近10% 但是理论上来说，在市场无量，题材多线轮动情况下，理应控手，无术无道[捂脸]    
>> 作者:   
走的是次新加送转加多题材叠加线
 

> 随风而动:   
恒铭达，上嵣路杭州帮。开盘一分后直接砸，我记住了。昨天竞价进场今天出来不够买酒。好好一手牌直接给干翻，一整天心情全沒了。    
>> 作者:   
准备去砸他么[呲牙]
 

> 小杨:   
昨打板恒銘达，今天水下-3%割肉……因为贪心，觉得涨少了再等等，最后受不了还是一刀割！心里最后郁闷至极转手打板铭普光磁……中间反复炸板，心态爆炸[捂脸]    
>> 作者:   
😂😂
 

> 夏一天:   
A叔指正 1.早盘稀土拉起，想打板儿宇晶犹豫放弃了。随板块止盈昨天跟的二傻子中铝。盘中仓控打德宏，不知溢价否？ 2.次新二板恒铭达开盘放量下砸，持有的叠加送转填权概念次新表现不佳，午后果断止盈。 3.早盘5G衍生概念多个低位上板儿，引导正宗高位概念股借机反弹出仓，没有接刀。收盘确认，反复考量铭普光磁，发现已打出高度，但是一直认为这个票为了减持而强怼，带有偏见而忽略不看。不知道我是不是错过了什么？ 4.农业套路一天，与其他板块相同，高位大长腿看到摸不到，怕摸到一手骚。 5全天盘面散乱无感，选股困难。午后低点潜伏低位杂毛，至尾盘获利4点。 敬请请指正[抱拳]    
>> 作者:   
一中铝今天走黄金线。应有红盘。三不算错。
 

> 谢沁桐:   
A师，今日盘面思考及操作，请点评。 1.昨日复盘。预判今日修复，把宇晶股份和彤程新材作为备选打板。忧虑点：题材不新，量能萎缩。 2.竞价思考。全峰汽车平开，超预期，预判0-3%。国泰集团一字。铭普光磁平开。汇源通信弱转强。金力永磁小低开。汇源通信竞价买点盈亏比如何，可否参与？ 3.盘面发展。金力永磁筹码浮躁，震荡后摸跌停。全峰汽车剧烈震荡，不敢追高或低吸。彤程新材上板，量能不够，放弃。理解为左侧大阴线压力大，不知道是否正确。宇晶股份一字炸板回封，本为计划买点，但有事错过。铭普光磁上板，汇源通信上板 4.操作。空仓 请教A师一个问题。铭普光磁作为中位股穿越，感觉切换很快，什么原因造成的？    
>> 作者:   
二只小试。三是部分原因，也是被卡位成后排，股性也不佳。四看留言[啤酒]
 

> 金798:   
A师的无私、宽容、友善是值得尊重的，无需计较某个别人的不满。虚心学习的就留在本群交流，反正我是学到了知识的，现在能够控制心态和把握情绪周期，最近基本都是赚钱出局，感恩A哥！    
>> 作者:   
[啤酒][啤酒]
 

> 多多:   
a叔好，昨天复盘有反弹预期，但金立提前动作怕第二天下砸又带坏情绪，加上节前综合征，犹豫的很，看来也是一见钟情的地方，往往犹豫的都没幸福    
>> 作者:   
[呲牙][呲牙]
 

> 朱sir:   
从大盘竞价变化来看，我感觉昨天大盘尾盘水下企稳倒更像是买点。昨天和今天师傅大金牙冒出的最有价值的金句，是有关龙头首阴的买点的暗示，为什么次日尾盘先下沉下为好，有心人可以找出来若干案例研究。不管你们懂了没有，反正我是信了，师傅点拨，徒弟自学，谢谢师傅！    

> 夏一天:   
A叔，我大胆的说一下，金力永磁的磁走了一波了。泉峰汽车又来了一波。单从名字上看磁》次》车，这也有可能是一条线。车我等了一周多了，也可能有臆想的成分。。[偷笑][流汗]今天盘面出了几个磁+车的股亮眼。    

> Roths:   
A叔好🔥1-开盘无量冲高出了金立...2-小仓低吸惠城—-要么跟低位稀土国泰宇晶要么跟近端泉峰...流通值小在大盘快缩量成2018年的情况下算优势吧比如万兴当年…盘中金立跳水它明显跟跌但挺住了；泉峰封涨它明显跟随但又不坚决回落均线—-这算脱离了金立嘛？尾盘集合上中简—-已经预期高挂还是最后一秒被1分钱给甩了没买到[流泪]明天就看泉峰&gt;惠城=中简了...盘后人民日报和发改委再提稀土...这是转不出去这个圈了吧惠城有预案加仓低吸买点吧？[愉快]    

> 奔跑的蜗牛:   
现在资金变聪明了，金力永磁明牌，全tm在低吸谁追账呢，量也放不出来    

> Lionger:   
A师几个地天板/大长腿带动的效应，今天大部分股属于长阳反包，感觉好难做。铭普光磁➡️全疯汽车➡️万向德农+敦煌种业    

> 崔凤波:   
竞价情绪良好，只是感觉现在抢的好凶，正常预期今天是有反弹点，方向大概还调整过的芯片，稀土，5g。指数无量，到了要选择方向的节点了，鉴于这种情况先空仓等待，现在就是封住了第二天不一定怎么样，封不住第二日大面的概率高很多。    

> ...:   
A叔，竞价看大盘情况不错，持股昨日仓控打板的明普光次，后在明普10点半后炸开之后除了，当时原因1、大盘量能没跟上2农业反抽吸金，原油板块反抽吸金，虽然当时两只5G三板股还是封死，但考虑到金立永兹7板高度，且10点半后砸入深水，遂卖掉明普后空仓，后午盘后五G相关个股助攻，后虽再破板也没有想再买回。收盘看早盘欣天科技错过，后就没有再动    

> 感动人生:   
A哥竞价不及预期开盘卖了高斯贝尔欣天科技锁仓没看国脉科技的盘子直接上了他华脉盘子小但是是三板恐高谢谢指点[啤酒][啤酒][啤酒][啤酒]    

> 虎子:   
A叔，昨天和今天一直在思考叔推演的，如果昨天尾盘金力永磁不拉涨停，今天大概率涨停板，甚至有二板预期。刚洗澡的时候突然有一些灵感：1、昨日尾盘如不拉，经过昨天换手筹码已达到平衡状态；2、今日涨停的驱动力就是龙头的反包预期以及情绪冰点的转折预期；3、今日的大阴线已彻底将筹码打散，龙头反包预期已兑现，筹码多属性纠缠，短期很难再理顺，大概率金力永磁周期结束，开启新的周期。洗澡有感，请A叔指教[合十][合十]    

> 云海:   
哥不必在意杠精，道法有缘者得之[啤酒][啤酒] [啤酒]个人理解市场情绪修复的手法切入方向，1是打造崩溃板块的长大腿，泉峰无带动效应的原因是物流军工承接低于预期，并未带起赚钱效应让情绪回暖，2就是上面说的，承接资金的题材赚钱效应太弱，没有带起情绪回暖，3是指数大涨带动整体情绪修复 [啤酒]哥[啤酒]的标准买点姿势是昨日泉峰跌停低吸，今日平盘再吸，金磁昨日如无拉升尾盘集合开仓，今日如连板加仓，很命大周一封住也是资金预判情绪修复的备选标，指数日常尿性，高开低走，热点散乱，资金各自为政，不是好的开仓日    

> 晨:   
叔好！ 早盘竞价氛围不错，稀土开的太强，情绪分时高点有回落预期，美力科技开盘后瀑布面，连班股谨慎接力，继续做大长腿预期，稀土回落后先锋新农开发先锋板，看种业种植业板块偏强，敦煌万象2选1两成仓上了敦煌，上午万象卡了敦煌人气心想明天能保本出即可，结果尾盘运气爆棚，直线拉板，真是撞大运了呢，收盘后发现此前公告有减持，早知道上午就不用这么费劲了。    

> 正兴五金蔡强:   
外围因素导致的高开，量却没放出来。那一定要慎重开仓。金立冲高出，手痒赌博性质的买了一层仓位泉峰，分时图走的惊心动魄[惊讶]运气不错版了。5g不想开仓，老鸨题材，铭普光磁看不懂不敢去。感觉昨天下午的金力真的邪门，板块氛围没有，大盘也不好，正常两点过后是会被砸盘的，尾盘是不是有人知道晚上有稀土消息[惊讶]    


