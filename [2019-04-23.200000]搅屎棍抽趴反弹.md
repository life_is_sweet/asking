### **搅屎棍抽趴反弹**
> 作者:A神asking   发表时间:2019-04-23 20:00:00   原文: [点击跳转](http://mp.weixin.qq.com/s?__biz=MzUyNTM3NDk5MQ==&amp;mid=2247484848&amp;idx=1&amp;sn=2437dd15b220712a2117a00718fcb83f&amp;chksm=fa1e46d0cd69cfc6d29da67c57680c3fcfcd09781b426eb3f9b2bb5a0ae35494428e72bee6c8&amp;scene=27#wechat_redirect) 

---
一直陆陆续续有新老朋友来，大家也蛮关心A叔的近况,可能大家都知道我的经历，现在当然早过了财务自由的阶段，这两年过着炒炒股票，打打德州，偶尔澳门一日游，腻了就去海上漂几天的日子。
股市没有神，我说的也不尽然全对。公号取名的时忘了告知小友助理，用A叔就好不要用神。所以以后大家留言，客气的话叫我声A哥或A叔就很好。
这里主要分享心得和交流逻辑，股市是一个自我修炼的地方，前辈们也最多给你们一些提醒，让你们少走点弯路，最大的敌人还是你们自己。而你会发现其实最终能帮你的也只有你自己，我不行！养家不行！小赵也不行！
—————博观而约取，厚积而薄发—————

![](./imgs/1585427163.2717743.png) 
核心个股题材属性和竞价预期
开盘
指数小阴开局，继续缩量。昨日为恐慌扩散期，今日低开幅度不够，恐慌杀跌盘等待宣泄。
预判继续下杀，如爆杀日内可能反转。如只是小杀，恐慌盘未出清会有反复。

午后中信建投搅屎，抽离反弹资金。指数诈暖后继续杀跌。

量能继续萎靡
主流题材末期
情绪继续回落

创投沉寂后反抽轮动，确定性不佳，板块高度有限。

新能源汽车亚星客车三板，补涨小龙红阳能源断板。板块政策加持仍在，本轮人气尚存，全面杀跌后看反弹。

工业大麻板块无涨停团灭，已连续爆杀两天，相对于新能源汽车板块，或先调先弹。低位首板或反包带动高位反弹概率大些。

次新永冠新材大跌，确认主流疲乏期板块卡位逻辑。最近次新贴上负属性标签，冰冻期是换手龙的孕育温床，个股发动初期在不断的预期差中上涨，打开高度的同时充分激活人气才有蜕变真龙机会。板块纳入持续观察。

如是纯粹的打板选手，今天单选逆回购。此阶段打板小肉大面，盈亏低，遇重仓踩雷亏钱更亏心态。

退潮爆杀初浪，反抽不远。
注意晚间公告，竞价是否有主流板块一字牵引。
低吸主流板块个股，地位及人气综合，配合卖压衰竭分时及龙虎榜单选股。

免责：本号所提个股仅为个人复盘所用，不推荐任何个股，不加个人QQ或者微信，亦没有任何QQ群或者微信群，股市有风险，投资需谨慎！！

![](./imgs/1585427163.3984365.png) 

我每天耐心的教，小友们有没有收获？
如果实在看不懂，就慢慢学。不懂又没耐心的话，劝君取关不浪费时间，因为炒短线不一定适合你。
如果有收获，可以点个赞，并帮我推荐转发给短线的同好，一起积累福报，也让我能够得偿回馈市场的心愿。

大家有问题可以踊跃留言
好的留言我基本都会回复

留言区有黄金！

![](./imgs/1585427163.5376554.png) 


---
> 崔凤波:   
正常看今天盘中应该是下杀反弹一下的节奏，拉指数打破了原有的节奏害了一批人。 指数小幅低开与大幅低开的区别在于恐慌程度不一样说的直白点就是很多人都没跑，都在等等看。先知先觉的开盘看到形势不对接着就跑，一旦有人领跑就会形成羊群效应，深看是资金的进出对情绪的影响。想到题材是一种载体，用来发酵情绪，情绪的发酵会带来资金的关注，大新强与一般的区别在于资金持续的关注程度。冰点是朦胧不知道哪里是方向，高潮有终点因为没有持续力，试错是为了寻找方向，叔的表格是最佳答案。 今天主看指数的走势情绪的位置，盘中人气股的表现，有没有涨停特别吸引人的个股原因是什么。综合看就是等老的反弹试错新的方向。    
>> 作者:   
值得置顶[强]
 

> 正兴五金蔡强:   
再也不想玩次新了，早上绿盘低吸的永冠新材（看到题材全灭，前20分钟爆了昨天的50%成交量）达到7亿多量时加的仓[尴尬]mmp[捂脸][捂脸][捂脸]像吃了屎一样难受    
>> 作者:   
等不想玩次新的再多点就差不多了
 

> 花生:   
A叔，今天高位股集体扑街，为什么不能看市北高新开启新的周期？是没有指数配合吗？    
>> 作者:   
老鸨能看几板😂
 

> 阳崇俊:   
A叔，我今天早盘领益智造分时回调，突破均线入一笔，上板加一笔，有问题吗？昨天入了富通鑫茂，今天看春兴精工午盘企稳，氢能源已经趴了，觉得5G下午应该有反抽，结果被闷看着跌停出不去[流泪]，这笔反思错在主观yy太多，加上没有设置好止损点…A叔我最近这么好的行情可是老是巨亏，要怎么做会比较好…[捂脸]    
>> 作者:   
这两笔是赌赢和赌输的差别
 

> Mr.AleX:   
A哥好，3个票的理解今天表述有点复杂，不乏意淫成分，请教下哥是否能如此理解： 1、永冠新材作为红阳能源周期内出现退潮情绪下的出口逻辑之一（老人气反抽也是出口逻辑之一，比如领益智造、市北高新），总体看在大局观里面稳定性是不及新周期（过度周期）试错阶段稳定性好，因为还有资金抱着老周期继续或者在挖掘补涨的想法（比如今天春兴精工早盘震荡不死想走妖，特发信息新高助攻，亚星客车、华昌化工新高补涨氢能源），导致资金情绪分歧大，特别在这种大级别周期可能和指数共振见顶阶段，就更容易出现和退潮期情绪一并陪葬概率。基于此，只是出口套利操作理解为主，就不会吃到今天的大面？有了大局观在结合技术看，今天烂板多次，分时走法纠结，并非强势分转一走法，即使今天10点49分回封买点，也会放弃？ 2、特发信息早盘缩量上板当下情绪环境肯定不去，但是盘面出现创投逻辑的演变，看了下特发信息的启动点和中钢天源一起的指数情绪转折日，理解可能走切换轮动抱团，结合他新高走法，春兴精工震荡，当时理解11点07分的回封是可以仓控试错的？尾盘是被春兴精工5g属性带下来的，但是回到大局观，昨天大佬斗地主强撸理解有背书红阳能源结束可能预期下春兴精工维系高度走回暖解救策略，基于此，特发信息符合技术买点，当下也是不能上的？ 3、梅雁吉祥启动日同特发信息，反倒不受题材拉扯，资金出口轮动成功？这些就是要提前做好做细的功课才能应对？    
>> 作者:   
一是。正常只有卖点。唯一的生机在于早盘看到魁齐路出局后分歧转一致。二放弃，题材轮动反抽的逻辑在这个阶段对打板支持有限。三不关注，今天二连才加自选做连板梯队观察[啤酒][啤酒]
 

> 悠悠岁月:   
a叔，今日打板特发信息，低吸特发信息，结果尾盘跳水，你看这个特发是不是取代东方通信成为下一个龙头    
>> 作者:   
服了你[捂脸][捂脸]
 

> 無我:   
看完复盘和评论区，不禁感叹：在现在这种指数爬坡的行情中，感觉炒股都这么难，你们以前都是怎么成功财务自由的？    
>> 作者:   
都是苦过来的[啤酒][啤酒]
 

> 咏而归:   
打建投瞬间，我以为我悟道了，和gjd站一起了，即将完成情绪逆转，骑着共振转势板走向人生巅峰…[色]5分钟后：隔夜摁[微笑]    
>> 作者:   
看量啊[捂脸][捂脸]
 

> 执念:   
哥，救我，本月两次过山车了，还剩3个点，炒股太难了[捂脸][捂脸][捂脸]国际实业我竟然没走掉，说出来我自己都不信，明天准备再杀一下试错了    
>> 作者:   
我也不信😂😂
 

> 尚品灯饰:   
叔，春兴精工上午一直在震荡，下午券商拉起，大盘反弹，突然想到5G龙头春兴精工会乘机拉起，加之昨天很多游资进去了，果断抢买，结果一路跌停，错在哪？    
>> 作者:   
昨天说过高位获利兑现和人气核心股双杀，还要去接盘[捂脸][捂脸]
 

> 王飞燕:   
冰冻期是换手龙的孕育温床。。。。。好好复盘，感受下龙位在哪里    
>> 作者:   
不着急，早一步就吃面[啤酒][啤酒]
 

> 晖洒自如:   
A叔，午盘时全柴跌停，红阳大跌，算冰点时间吗？全柴破板买入，有错吗？谢谢！    
>> 作者:   
已经先调的不搞去全柴😂😂
 

> Eternity:   
叔好，中信拉升带动大盘，怎么样在盘中判断是转势板还是搅势棍？    
>> 作者:   
量能支持么
 

> 童吉:   
A哥好，怎么判断魁奇路出局？    
>> 作者:   
盘前统计持筹，盘口看成交细单。
 

> 铷妍:   
为了防止自己乱买，下午睡觉了。两点起来看盘[呲牙]如果特发信息不炸板，也许会跟着试错一点春兴，，特发信息砸板就不想加仓位了。关电脑看韩剧！    
>> 作者:   
我是看的很开心[呲牙]
 

> Rangor:   
A叔好，这次记住退潮初期的应对忍手了。今天有事没法看盘（平时也是手机看），快9.25时赶紧把车停路边，竞价把西安旅游跑了。然后开盘竞价看上证大幅缩量，然后也没有什么主流，就等下杀低吸了。结果因为着急开车，第一波下杀就吸了一手诺普信，差不多的第一次低点吸了点福安药业就去忙了。结果都是凉凉，后面都有跌停，明显不是恐慌。请问A叔，怎么看崩溃盘或者是恐慌盘，还是体会不好，看不好。虽然买了一点，还不如空仓好。 还有，今天看，自己昨天买股就是赌。得好好学习A叔的分析，哪天A叔不写了自己也能分析出什么时期怎么应对。    
>> 作者:   
看你计划是吸的是恐慌，还没爆杀急什么.择股可以，择时[捂脸][捂脸]
 

> 清淡:   
A叔寥寥数语，竟是如此真谛！我虽未悟道，但是感恩A叔如此真诚相授，请受我一拜！！！    
>> 作者:   
🙏🙏
 

> 又一村:   
A哥晚上好，大长腿是不是一般出现在情绪退潮初期？最近是不是要出现了。A哥能不能再说一下，题材炒作，情绪炒作，筹码炒作三者的区别[西瓜][西瓜]，题材周期炒作和情绪周期炒作两者不是共生存在的吗，筹码跟他们不是同个系列吧[捂脸]，搞糊涂了，为什么A哥要我们选择其中一个，感谢感谢[玫瑰][玫瑰][玫瑰][玫瑰]。。    
>> 作者:   
做个菜还有色香味，配合好环境，好心情，还有对的人。炒股当然更复杂[啤酒][啤酒]
 

> 袅袅寒烟จุ๊บ:   
哥晚上好，竞价缩量是开盘后一分钟的数据还是25分钟时的数据？    
>> 作者:   
竞价对竞价同比，开盘一分钟对开盘一分钟同比，看你的软件什么方便和自己习惯，效果一样[啤酒][啤酒]
 

> 畅游:   
请教A师，作为小散的我们都可以看出来量能不够，强拉券商就是作死，为何市场那些大资金还去做搅屎棍吗？它们是故意为之，还是真不懂盘面而当了搅屎棍？    
>> 作者:   
总不见得一直跌，总要抽动一下。
 

> 大道至简:   
叔，尾盘低吸东方创业。这时侯，做尾盘低吸套利是否更好。    
>> 作者:   
筹码已经稳定，今创投近十个涨停，午后杀不动小试。
 

> EaSon.D&J🎻:   
A哥，今天保持空仓忍手。光看不买。可以打几分？    
>> 作者:   
90[啤酒][啤酒]
 

> 王飞燕:   
哥，现在对情绪周期有点眉目了（唉，笨哪，快一年了才有点头绪）。但对新周期如何判断方向与题材，该怎么着手，还不会。这方面该怎么补缺啊[啤酒]    
>> 作者:   
也一直在说，都不仔细看[呲牙]
 

> 青雲:   
A哥好，昨天打板永冠和通光，今天涨停附近出永冠，通光昨晚出减持利空，本应开盘就该走，却一拖再拖，最后走不了，犯了短线大忌。有人说打涨停板第二日百分之九十都该走掉，这个比例A哥怎么看？祝A哥福如东海[抱拳]    
>> 作者:   
你说的是纯打板的模式，打的是分仓概率。龙头战法一夜情概率略低些。
 

> 张克梁:   
A哥，您的操作首板不关注对吗，只专注二板以上接力吗？    
>> 作者:   
现在的市场跟主流资金一定是看连板，连板少才看首板。
 

> 点睛:   
现阶段主流板块都不知道是哪个？    
>> 作者:   
氢麻
 

> 成王败寇:   
大佬就是大佬，所有人的的复盘都带恐慌色彩，a神却道出机会点，尾盘低吸某板块占个先手    
>> 作者:   
全面反抽资金需求量太大看不到，局部机会，持仓如走空也不能留恋[啤酒][啤酒]
 

> 浅笑😘:   
叔看了评论臆测意淫盘好多啊。“量即生命。逢情绪下降周期中，当量能不足以支撑盘面多题材或主流题材主升，就是风险降临时。”“趋势的力量引导市场合力，对势的理解超过一切”靠叔点拨空仓静待次新[奸笑]    
>> 作者:   
都记住啦[啤酒][啤酒][啤酒]
 

> 蔡:   
师傅，永冠在10:50回封的时候，换手50左右，应该够了，板块效应其实还可以，是不是有庄暗算？    
>> 作者:   
分时不正常，看不懂。是不是有吹票的？
 

> 刘红红:   
A师： 1.上次西安旅游与连云港两天走完的路，今天美都能源与深冷股份要1天走完，资金太急了。 2.力帆股份、全柴动力、中钢天源、岳阳林纸、福安药业、置信电气、春兴精工跌停，一顿乱撬，资金太分散；凯龙股份缩量跌停，竟然没资金撬，看好；中钢天源板上换手还可以，看好。 3.多属性的恒泰艾普被天然气带崩了且竞价就被德美化工卡了位；特发信息被5G带崩了；看来多属性个股在题材启动期可以卡位是利好，在退潮期可能被带崩，真是一把双刃剑。还是市北高新稳。 4.美度能源亚星客车华昌化工，板块梯队完整，这是要玩穿越吗？    
>> 作者:   
觉得是穿地雷阵😂
 

> 志敏:   
A叔好，龙盛怎么看？    
>> 作者:   
长线
 

> 雨田水凌:   
今天深冷股份的画面感太强了，上天入地，无所不能啊！幸好心脏健康。[捂脸][捂脸][啤酒][啤酒][啤酒]    
>> 作者:   
看得我也😱😱
 

> 云淡风轻:   
A哥永冠赚了差不多百分之30没有卖以为成妖贪心了想请教一下今天的分时图是不是钓鱼线出货法    
>> 作者:   
钓鱼线是砸出来的，因果反了。
 

> 一念之间.:   
拿了三天的富联，卖在翻红前。我是不是傻，今天抽了自己两巴掌[抓狂]    
>> 作者:   
[捂脸][捂脸]
 

> Chen:   
今天水下低吸永冠，板上加仓，满脸泪两行    
>> 作者:   
[捂脸][捂脸]
 

> 立:   
A叔晚上好，今天低吸的几个标的。大麻提前调整是优选板块，板块中顺灏股份最先调整的老龙头，是最优选，其次是福安药业有人气.氢能源板块，只有美锦能源是先调整的人气股可以低吸，但考虑龙虎榜，大资金还没走，应该放弃。这样理解对吗？    
>> 作者:   
[啤酒][啤酒][啤酒]
 

> 乾坤娇鹰:   
A兄好，情绪退潮期忍手观察是关键。今日止盈春精36个点，竞价看华昌超预期，开板后追跌14.30急吸三成，000702竞价超预期，9.31分挂8.20二成。。。[抱拳][抱拳][抱拳]敬请指正。    
>> 作者:   
仓位放轻[啤酒][啤酒]
 

> Mr.Xu:   
叔，今日平盘清了春兴，板砸中信建投，尾盘低吸顺灏收盘就赚2个点[憨笑]感谢叔的教导    
>> 作者:   
[啤酒][啤酒]
 

> 金798:   
今日强势股全面回调，只有猪肉及业绩增长票上涨，40多家跌停，退潮期一定要回避。 今日操作： 1、正虹科技回封小仓打板。 2、华通医药借壳，题材量级大，平盘及水下分批低吸，其实低吸更考验理解力，难度更大，还是打板容易些！但今天华通应系被情绪带下去的，不知道理解对否？    
>> 作者:   
是。
 

> 至爱康小妖:   
叔，你最近讲的这个一字牵引很有用啊，难道就没有人重视起来? 1.一字牵引既可以替大哥稳情绪，引导资金保板块，又可防止情绪回落太突然，给深深介入的资金一个缓冲，最近看氢能才真正的感受到资金围魏救赵操的一手好盘啊，叔，我这理解对否? 2.叔，你每到一个阶段细细念的名词跟买点是不是就是当下市场的核心套路？ 3.我最近看龙虎榜结合分时看，最近高位的杀跌，大多都是获利盘兑现居多，所以感觉杀的走势并不是非常恐慌，所以个人感觉，这个时候去吸崩溃盘，盈亏比一般般，情绪退潮，没有最低只有更低，这个理解对否？ 4.叔，我今天思考一个问题，低吸崩溃盘的本质是不是在找一个人气与市场情绪的平衡点，既人气未尽，情绪仍在?    
>> 作者:   
说很久了，之前流水帐里天天有。二我也有看不到的。三所以要等双杀。四情绪先崩，人气未尽。
 

> 今天:   
大老在打市场情绪为后面的反抽做准备吧真正的大冰点在后面反抽完了后的再次市场垮陷入绝望吧    
>> 作者:   
是[啤酒]
 

> 如梦如幻:   
老师好，今天尾盘红阳能源拉升，是否可以跟进，属于情绪崩溃低吸吗？谢谢    
>> 作者:   
只吸不追
 

> 赫驰猜想:   
卖飞领益和市北，上了搅屎棍，是不是不及格[流泪]    
>> 作者:   
[流汗][流汗]
 

> EaSon.D&J🎻:   
看到中信建投拉伸。感觉恐慌释放不够。且没有券商跟随。果断忍手。    
>> 作者:   
[啤酒][啤酒]
 

> 锋:   
A叔，今日操作如下：盘前思考，大盘今日应有下杀，20日线附近可博弈。方向是5G+创投。竞价来看，题材股基本歇菜，创投德美化工一字封板，资金回流创投的可能性大。1.市北高新承接良好，低吸一笔。 2.9点45分特发信息量能承接良好二封的时候仓控一笔。 3.9点56美都能源量能承接良好，仓控一笔。 A叔，我这三笔操作可有问题？还是说情绪退潮忍收手为主？特发信息崩了，是被春兴精工5G板块带开的？谢谢叔的解惑    
>> 作者:   
概率上给的策略是只吸崩溃
 

> blueidea:   
根据老师的指引，今日看懂了主流杀跌之后市场恐慌下低吸前回调到位的人气股逻辑，顺灏股份符合这一标准，不过没有操作，退潮期还是没有管住手，去试错了小题材的龙头，大面一碗，明日只能割肉了，老师说了退潮期小题材不去，这两天算是接受了教训，交学费买经验[捂脸]    
>> 作者:   
🙏🙏
 

> 陈默:   
请教a师两个问题盼回复。一是牛股二波不是都是从二板起步么，怎么市北高新二板起步就是老鸨呢？东方通信不也是这么来的么？怎么此一时彼一时呢？大盘不在主升浪么？第二个问题是券商启动要看大盘的量么？感觉不太对吧，应该是券商先封涨停，后续券商继续封涨停，大盘量自然就上来了，不能本末倒置吧？请a师赐教    
>> 作者:   
题材炒太多次没有新鲜度，上高度比较难。没量券商封不住涨停
 

> 大星星:   
A叔，继续周期问题 1.这轮精准、龙盛和美景的周期是上一个大周期的补涨小周期还是下一个大周期的启动小周期?总是搞不清楚这个节点如何划分。 2.如果是属于上个大周期的，那这个退朝期是否可以看做是上个大周期的整体退潮的末端?如果是属于新的大周期那这个退朝应该不会有大冰点?    
>> 作者:   
二前者概率大[啤酒]
 

> 向着阳光的树:   
A叔好,昨日你提醒题材退潮，今早就想好了盘中如有工业大麻和氢能源的个股涨停不去买入，只做关注。采掘板块和次新加入备选。采掘板块恒泰艾普一字，贝肯能源、洲际油气开盘先后冲板（首封我出去工作了，回来后发现这2个股破板在高位震荡）当时思考谁先回封买谁，9：44分恒泰一字打开，看见后马上放弃这个板块。请教A叔这个回封和看见恒泰一字打开的思考可以吗？盘中观察到次新永冠有个冲板动作首封没去（考虑到该股在高位，且今日的其他封板个股都不稳定，怕炸板），当时马上回想A叔的次新情绪，有连板、有冲高、又封板，固选着了相对低位的福莱特低吸。请教A叔这个思考可以吗？    
>> 作者:   
也说过低级别题材不参与[啤酒]
 

> 董二家的大小姐:   
叔叔晚上好了昨天是特别想进次新2板今天是真不想进了这里下午啦证券的时候我已安心的做飞机上从清迈飞向普吉岛了希望行情等等我下周再起来吧    
>> 作者:   
不急，节后再看
 

> Climber:   
【工信部新闻发言人：将大力推进我国氢能及燃料电池汽车产业创新发展】财联社4月23日讯，工信部新闻发言人黄利斌表示，下一步，我们将进一步加大工作力度，联合有关部门开展示范运行，破解氢燃料电池汽车产业化、商业化难题，大力推进我国氢能及燃料电池汽车产业的创新发展。    

> 执念:   
哥，看见个搞笑的段子，大清（氢）亡了[呲牙][呲牙]    
>> 作者:   
😂😂
 

> 随风而动:   
今天+4+2分二批走掉红日，开盘全柴凯龙深水区作业。今天的红阳己计划止盈，就看诱多的成分有多高[呲牙]。赌场不关门，资金永不瞑。等情绪指数周期的共振再来把大的[调皮]    
>> 作者:   
[啤酒][啤酒][啤酒]
 

> 朱sir:   
看各位面吃的都要吐了，就不能学学A师教的低吸节奏的把握吗？强势人气股回抽一样能套利，非得每天打板打的要死要活的。[呲牙]    

> 方宇阳:   
A叔，今天继续空仓。不过感觉早上2054一字牵引，似乎可以考虑仓控做第一个跟随上板的2045。考虑高位股情绪未释放，早盘最后还是选择放弃机会    
>> 作者:   
舍才有得[啤酒][啤酒]
 

> 厚德载物:   
A哥终于有个新奇发现中信建投真是搅屎棍本来深冷股份地天板板的好好的13点半中信直线拉升深冷立马投降直线下挫一颗老鼠屎坏了反抽的一锅汤    
>> 作者:   
原来流水帐里写，现在你们要自己看[啤酒][啤酒]
 

> 糊涂虫:   
A哥，根据前几波总结，龙头亏掉两个板以后再开盘下杀才会有反弹机会，今天您说今天爆杀就有机会，那之前两个跌停为什么都没有恐慌呢？有没有哪个当天爆杀反弹的例子让我再研究一下？ 另外海底光缆当天n个一字板，您表格只列出通光线缆，结果第二天只有它涨停，能请教一下看中它的原因吗？    
>> 作者:   
东方网络不是么？你这记性还不如老年人😂
 

> 追忆似水年华，:   
今天就是把昨天低吸的永冠与五丰不断地了结，盘面太差了，没有开仓欲望，等待冰点的新题材试错    

> 徐:   
老师辛苦了！今日高位股补跌加指数回调，能力不足怕吃面没有操作，跟着老师继续学习修炼，谢谢老师！    

> 阿盼:   
a师，今天早上看次新板块还可以，下杀绿盘低吸了永冠，上板笑嘻嘻，午盘mmp，低吸的通光线缆也是一嘴毛。昨日预判退潮期来临，只留了一层仓，今天没忍住手又是大半仓，对于退潮期的凶猛没有足够的认识，看来面还没吃够[流汗][流汗]，道阻且长[流泪][流泪]    

> Roger 姜:   
上周二反转拉的是银行稳定指数后带起情绪，这次开始拉的白酒，稳定指数，情绪感觉有些回暖，但是有点跟的量不太够，回踩一下资金再拉证券没道理啊，这是逼死的套路，直接把高位股再拉跳水。而且这是最近第三次玩反转了，一次比一次弱，资金都不信了，[撇嘴]    

> 修心见性:   
A哥好:多谢哥急的喂进了嘴。大妈反包是267，反抽是194，首板看好565。氢先看758。欢迎反驳……    

> 初一:   
昨天开盘卖飞福莱特，今天开盘微红出昨天预判的中信建投。。。。最近操作好秀[流泪]    

> 尤志鑫:   
报告A叔，退潮期只吸不追高，最近一笔浙江龙盛龙低吸4月17日拉升时卖出赚10点，收盘前有认为可能模仿恒立实业当天拉升当天洗盘，第二天直接高开拉升，所以又买回一半，可惜第二天早盘低开不及预期直接出了错失6个点，上周四仓控吸威派格，周一临近午盘又吓出恐荒盘，分批加了3倍仓，周一下午V起，走了首仓，今天红盘走了昨天的加仓，递势收获5点，今天轻仓进了鼎捷软件和启迪古汉，目前还没什么盈亏    

> lucky:   
A叔晚上好[合十][合十][合十]看留言区及叔的答复都找到了自己疑惑点的答案，感谢[啤酒][啤酒][啤酒] 师兄师姐好厉害[强]努力学习，争取早日进阶[强壮][强壮][强壮]    

> 今天:   
创投也是助攻气氢一字的德美化工带氢属性要出地天要出反抽板应该是从这个方向先出今天盘中几只形态引导做地天票因为证券大股拉了下人气分流掉了没成功悬念留给了明天    

> 罗土木:   
今天的永冠刷三观，一、我没有高开，二、我经历了换手涨停，三、有点小跟风.照样挂了，80%的换手    

> 守拙的老农:   
不为别的，就看a叔这流畅简洁的复盘都相当舒服[呲牙]    

> 爱的钢琴手🎹:   
A叔，永冠新材分时有哪里不正常，学生看不出来啊，另外卖压衰竭分时应该是什么样？学生理解为：下跌缩量，比较抗跌。大麻的紫鑫药业、顺灏股份有这样的意思。忘恩师点拨。    


